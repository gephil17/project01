
//要加的東西
let dishResult = ["一", "二", "三", "四", "五"]

//次數
let a = dishResult.length
let answer = '';
for (let x = 0; a > 0; a--) {
    answer += dishResult[a-1]
}
// let b = 0
// while (a > 0) {
//     b += 1;
//     a = a-1;
// }

// console.log(b)




// for (let x = 0; x < dishResult.length; x++) {
//     answer = dishResult[a-1] + dishResult[a-2]
//     a --
// }



// let input = "被加的數"
// let answer = input + dishResult[1] + dishResult[2]


// Antoshimo Cafe & Bakery
// Aroma Dessert Cafe
// Beans荳子
// Between Haru 一日之間
// Book B Kitchen Mealmeal
// Diff. Cafe
// hōmu by favilla
// KoKo Coffee Roasters
// Marka House
// No Boundary
// RETOLD coffee.tea.spirits
// Return Coffee 362
// TAP The Ale Project



// let abc = "1,2,3,4,5"
// let usersArr = abc.split(',')
// for (let user of usersArr) {
//     console.log(user)
// }

// project01=# select * from restaurants inner join districts on restaurants.district_id = districts.id ;
// ERROR:  column restaurants.district_id does not exist
// LINE 1: select * from restaurants inner join districts on restaurant...
//                                                           ^
// HINT:  Perhaps you meant to reference the column "restaurants.district".
// project01=# \d+ restaurants;
// project01=# select * from restaurants inner join restaurant_dish on restaurants.id = restaurant_dish.restaurant.id;ERROR:  missing FROM-clause entry for table "restaurant"
// LINE 1: ...ts inner join restaurant_dish on restaurants.id = restaurant...
//                                                              ^
// project01=# select * from restaurants inner join restaurant_dish on restaurants.id = restaurant_dish.restaurant_id;project01=# select * from restaurants inner join restaurant_dish on restaurants.id = restaurant_dish.restaurant_id inner join dish on restaurant_dish.dish_id = dish.id;
// ERROR:  relation "dish" does not exist
// LINE 1: ...nts.id = restaurant_dish.restaurant_id inner join dish on re...
//                                                              ^
// project01=# select * from restaurants inner join restaurant_dish on restaurants.id = restaurant_dish.restaurant_id inner join dish on restaurant_dish.dish_id = dishs.id;
// ERROR:  relation "dish" does not exist
// LINE 1: ...nts.id = restaurant_dish.restaurant_id inner join dish on re...
//                                                              ^
// project01=# select * from restaurants inner join restaurant_dish on restaurants.id = restaurant_dish.restaurant_id inner join dish on restaurant_dish.dish_id = dishes.id;
// ERROR:  relation "dish" does not exist
// LINE 1: ...nts.id = restaurant_dish.restaurant_id inner join dish on re...
//                                                              ^
select * from restaurants 
inner join restaurant_dish 
on restaurants.id = restaurant_dish.restaurant_id 
inner join dishes 
on restaurant_dish.dish_id = dishes.id;


select * from restaurants 
inner join restaurant_dish 
on restaurants.id = restaurant_dish.restaurant_id 
inner join dishes 
on restaurant_dish.dish_id = dishes.id 
where dishes.dish in ('Cafe','西式');


select distinct restaurants from restaurants 
inner join restaurant_dish 
on restaurants.id = restaurant_dish.restaurant_id 
inner join dishes 
on restaurant_dish.dish_id = dishes.id 
where dishes.dish in ('Cafe','西式')
and restaurants.district = '荃灣';



console.log(locationReq)
console.log(locationResult)

元朗,荃灣
[ '元朗', '荃灣' ]



console.log(dishReq)
console.log(dishResult)

Cafe,西式
[ 'Cafe', '西式' ]




// this work in postsql
select distinct restaurants from restaurants 
                    inner join restaurant_dish 
                    on restaurants.id = restaurant_dish.restaurant_id 
                    inner join dishes 
                    on restaurant_dish.dish_id = dishes.id 
                    where dishes.dish in ('Cafe','西式')
                    and restaurants.district in ('元朗', '荃灣');
//////////////////////////////
// this also work!!!!!!!111
select distinct name, address from restaurants 
                    inner join restaurant_dish 
                    on restaurants.id = restaurant_dish.restaurant_id 
                    inner join dishes 
                    on restaurant_dish.dish_id = dishes.id 
                    where dishes.dish in ('Cafe','西式')
                    and restaurants.district in ('元朗', '荃灣');
                    
let sql = format(`select distinct name, address from restaurants 
                inner join restaurant_dish 
                on restaurants.id = restaurant_dish.restaurant_id 
                inner join dishes 
                on restaurant_dish.dish_id = dishes.id
                where restaurants.district in (%L)
                and dishes.dish in (%L)`, locationResult, dishResult)
console.log(sql)
let testing = await client.query(sql)


//用full outer join!!!!!!!!!!!!!!!!!!!
//用inner join 的話, 如果餐廳冇dishes裡面嘅菜式, 就唔會show出黎!!!!!!!!
select distinct restaurants.name, restaurants.address from restaurants 
                    full outer join restaurant_dish 
                    on restaurants.id = restaurant_dish.restaurant_id 
                    full outer join dishes 
                    on restaurant_dish.dish_id = dishes.id 

                    where restaurants.district in ('元朗')
                    or dishes.dish in ('元朗');




locationResult = [ '元朗', '荃灣' ]
dishResult = [ 'Cafe', '西式' ]
// fail no result at all
`select distinct restaurants from restaurants 
                                    inner join restaurant_dish 
                                    on restaurants.id = restaurant_dish.restaurant_id 
                                    inner join dishes 
                                    on restaurant_dish.dish_id = dishes.id 
                                    where dishes.dish = any (Array ${locationResult})
                                    and restaurants.district = any (Array ${dishResult})`

// error: column "元朗" does not exist
`select distinct restaurants from restaurants 
                                    inner join restaurant_dish 
                                    on restaurants.id = restaurant_dish.restaurant_id 
                                    inner join dishes 
                                    on restaurant_dish.dish_id = dishes.id 
                                    where dishes.dish in (${locationResult})
                                    and restaurants.district in (${dishResult})`
