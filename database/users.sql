
CREATE TABLE users (
    id SERIAL primary key,
    name VARCHAR(255) not null,
    email VARCHAR(255) not null,
    password VARCHAR(60) not null,
    profile_picture VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE events (
    id SERIAL primary key,
    name VARCHAR(255) not null,
    creator_id INTEGER not null,
    FOREIGN KEY (creator_id) REFERENCES users(id),
    vote_deadline VARCHAR(10),
    picture VARCHAR(255),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE participations (
    id SERIAL primary key,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    event_id INTEGER not null,
    FOREIGN KEY (event_id) REFERENCES events(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE messages (
    id SERIAL primary key,
    content TEXT not null,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    event_id INTEGER not null,
    FOREIGN KEY (event_id) REFERENCES events(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE private_message (
    id SERIAL primary key,
    sender_id INTEGER not null,
    FOREIGN KEY (sender_id) REFERENCES users(id),
    content TEXT not null,
    receiver_id INTEGER not null,
    FOREIGN KEY (receiver_id) REFERENCES users(id),
    created_at TIMESTAMP DEFAULT now() not null,
    updated_at TIMESTAMP
);


CREATE TABLE event_dates (
    id SERIAL primary key,
    date VARCHAR(10) not null,
    event_id INTEGER not null,
    FOREIGN KEY (event_id) REFERENCES events(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE event_date_votes (
    id SERIAL primary key,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    event_date_id integer not null,
    FOREIGN KEY (event_date_id) REFERENCES event_dates(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE event_venues (
    id SERIAL primary key,
    restaurant_id integer not null,
    FOREIGN KEY (restaurant_id) REFERENCES restaurants(id),
    event_id INTEGER not null,
    FOREIGN KEY (event_id) REFERENCES events(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE event_venue_votes (
    id SERIAL primary key,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    favoured BOOLEAN,
    event_venue_id integer not null,
    FOREIGN KEY (event_venue_id) REFERENCES event_venues(id),
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE friendship (
    id SERIAL primary key,
    adder_id INTEGER not null,
    FOREIGN KEY (adder_id) REFERENCES users(id),
    receiver_id INTEGER not null,
    FOREIGN KEY (receiver_id) REFERENCES users(id),
    status VARCHAR(255) not null,
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

CREATE TABLE personal_schedule (
    id SERIAL primary key,
    user_id INTEGER not null,
    FOREIGN KEY (user_id) REFERENCES users(id),
    date VARCHAR(10) not null,
    content TEXT,
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP
);

alter table event_date_votes ADD COLUMN availability BOOLEAN not null;
alter table event_venue_votes ADD COLUMN favoured BOOLEAN;
alter table users ADD COLUMN profile_picture VARCHAR(255);
alter table event_dates ALTER COLUMN date TYPE VARCHAR(10);
alter table events ADD COLUMN vote_deadline VARCHAR(10);
alter table event_venue_votes alter column favoured type boolean drop not null;
alter table events ADD COLUMN picture VARCHAR(255);




////////////////////////////////////////////////////////ignore
 id | adder_id | receiver_id |
----+----------+-------------+
  2 |        1 |           8 | 
  3 |        1 |          11 | 
========== 另外有一個users table ========

==========  code  =============              =======  intepretation ========
select fdship.id,                            選擇fdship裡面嘅id, fdship裡面一定要有個叫id嘅column
       adder.name as adder_name,             選擇adder裡面嘅name, adder裡面一定要有個叫name嘅column
       receiver.name as receiver_name        選擇receiver裡面嘅name, receiver裡面一定要有個叫name嘅column
from   friendship fdship                     將friendship呢個table當成fdship
join   users adder                           將users呢個table當成adder, join fdship
on     fdship.adder_id = adder.id            fdship join adder的方法
join   users receiver                        將users呢個table當成receiver, join fdship
on     fdship.receiver_id = receiver.id;     fdship join receiver的方法


============  result table ===============
 id | adder_name | receiver_name
----+------------+---------------
  2 | henrytsang | test1
  3 | henrytsang | alice

  let result = await client.query(`select adder_id from friendship where `)
=======





prepare
開定兩個ac  (prepare1, prepare2)
開定兩個browser, login定

register 1 個 ac 
轉相, dark

direct to home page (blank)
demonstrate create event
    1.  need fd and restaurants
    2.  add fd
            search fd
            pm fd
            add one more fd
    3.  search restaurant
            get a restaurant name
back to create event 
    1.  create an event



direct to individual event page
    1. select and vote date function
    2. vote venues and add more venues function
    3. group messages
    4. add friend into event

direct home page
    1. display in latest events
    2. personal scheduling

invite audience to play, sd ip a