import pg from 'pg';
import dotenv from 'dotenv';
// import jsonfile from 'jsonfile';
dotenv.config();


// for henry selfish use
export const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

async function connectDB() {
    await client.connect();
    console.log("connected")
}

connectDB()


// async function importDataToDB() {   
//     let readJson = await jsonfile.readFile("restaurant.json", "utf8")
//     // console.log(readJson)
//     for (let i = 0; i < readJson.length; i++) {
//         await client.query(`INSERT INTO restaurants (name, address, style, territory, district) VALUES ($1, $2, $3, $4, $5)`, [readJson[i].name, readJson[i].address, readJson[i].style, readJson[i].territory, readJson[i].district]);
//     }
//     console.log("done")
// }
// importDataToDB()

// async function searchTW() {
//     let answer = await client.query(`select * from restaurants where id < 5`);
//     console.log(answer)
//     console.log(typeof answer.rows[0].address)

// }

// searchTW()

// async function insertRestIDToRDTable() {
//     let answer = await client.query(`select id from restaurants`);
//     let idArray = []
//     for (let row of answer.rows) {
//         // await client.query(`INSERT INTO restaurants (name, address, district) VALUES ($1, $2, $3)`, [readJson[i].name, readJson[i].address, readJson[i].district])
//         idArray.push(row)
//     }
//     for (let i = 0; i < idArray.length; i ++) {
//         await client.query(`INSERT INTO restaurants_districts (restaurant_id, district_id) VALUES ($1, $2)`, [idArray[i].id, 3]);
//     }
//     console.log("done")
// }
// insertRestIDToRDTable()

