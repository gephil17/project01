!!!!!!!!real restaurants
// create table
create table restaurants (
    id serial primary key,
    name varchar(255),
    address varchar(255),
    style varchar(255),
    territory varchar(255),
    district varchar(255)
);

// insert data from json to database
// async function importDataToDB() {   
//     let readJson = await jsonfile.readFile("restaurant.json", "utf8")
//     // console.log(readJson)
//     for (let i = 0; i < readJson.length; i++) {
//         await client.query(`INSERT INTO restaurants (name, address, style, territory, district) VALUES ($1, $2, $3, $4, $5)`, [readJson[i].name, readJson[i].address, readJson[i].style, readJson[i].territory, readJson[i].district]);
//     }
//     console.log("done")
// }
// importDataToDB()

!!!!!!!!!!!!!!!!!!!!!!!!!!!AFTER LUNCH!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

////////////////////////////////////////////////// district information
// create and insert CONSTANT data into THREE tables
create table territories (
    id serial primary key,
    name varchar(50)
);
insert into territories (name) values ('香港'), ('九龍'), ('新界');

create table districts (
    id serial primary key,
    name varchar(50),
    territory_id integer,
    foreign key (territory_id) references territories(id)
);
insert into districts (name, territory_id) 
values ('上環', 1), ('中環', 1), ('旺角', 2), ('尖沙咀', 2), ('元朗', 3), ('荃灣', 3);








//////////////////////////SPARE
select * from restaurants
    inner join restaurant_dish
        on restaurants.id = restaurant_dish.restaurant_id
    inner join dishes
        on restaurant_dish.dish_id = dishes.id
where dishes.dish = '日本菜' and restaurants.district = '荃灣';


/////////////////////////////////////////////////////////to be confirmed
// create table
create table dishes (
    id serial primary key,
    dish varchar(255)
);
insert into dishes (dish) values ('日本菜'), ('茶餐廳'), ('火鍋'), ('飲品'), ('西式'), ('海鮮');

create table restaurant_dish (
    id serial primary key,
    restaurant_id integer,
    FOREIGN KEY (restaurant_id) REFERENCES restaurants(id),
    dish_id integer,
    FOREIGN KEY (dish_id) REFERENCES dishes(id)
);
insert into restaurant_dish (restaurant_id, dish_id) 
-- select id from restaurants where style like '%日本菜%', 1;
values ((select id from restaurants where style like '%日本菜%'), 1);


create table restaurant_district (
    id serial primary key,
    restaurant_id integer,
    FOREIGN KEY (restaurant_id) REFERENCES restaurants(id),
    district_id integer,
    FOREIGN KEY (district_id) REFERENCES districts(id)
);

//////////QUESTION!!!!!!!!!!!!!!!!!!!!!!!



//ERROR:  more than one row returned by a subquery used as an expression
insert into restaurant_district (restaurant_id, district_id) 
select id from restaurants where district = '上環', 

// 先insert 一個column, 再Update另一個column
update restaurant_district
set district_id = 1;
//////////







insert into restaurant_district (restaurant_id) 
select id from restaurants where district = '中環';
//
update restaurant_district
set district_id = 2
where id > 29;
//////////
insert into restaurant_district (restaurant_id) 
select id from restaurants where district = '旺角';
//
update restaurant_district
set district_id = 3
where id > 53;
///////////////
insert into restaurant_district (restaurant_id) 
select id from restaurants where district = '尖沙咀';
//
update restaurant_district
set district_id = 4
where id > 77 ;
/////////////
insert into restaurant_district (restaurant_id) 
select id from restaurants where district = '元朗';
//
update restaurant_district
set district_id = 5
where id > 101 ;
/////////////////
insert into restaurant_district (restaurant_id) 
select id from restaurants where district = '荃灣';
//
update restaurant_district
set district_id = 6
where id > 125 ;
/////////////////


  1 | 上環   |            
  2 | 中環   |            
  3 | 旺角   |            
  4 | 尖沙咀 |            
  5 | 元朗   |            
  6 | 荃灣   |            

  SELECT 
   COUNT(*) 
FROM 
   restaurant_district
WHERE
   district_id = 1;