import express from 'express';
import { Request, Response, NextFunction } from 'express';
import expressSession from 'express-session'
import bodyParser from 'body-parser';
import path from 'path';
// import jsonfile from 'jsonfile';
import multer from 'multer';
import { Client, QueryResult } from 'pg';
import dotenv from 'dotenv';
import * as bcrypt from 'bcryptjs';
import http from 'http';
import {Server as SocketIO} from 'socket.io';
// import session from 'express-session';
import format = require('pg-format');


dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

client.connect()
    .then(() => {
        console.log(`DB ${process.env.DB_NAME} Connected.`)
    })
    .catch(err => {
        console.log("DB Disconnected.")
    })


const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.use(`/:table`, async (req, res, next)=>{
//     let existingTableList = await client.query(`
//     SELECT tablename
//     FROM pg_tables
//     WHERE schemaname = 'public'
//     `);
//     let existingTables = existingTableList.rows.forEach(el => )

//     if (req.method == "GET"){
//         let table = req.params.table;
//         console.log("select", table, "where", req.query);
//     }
//     next();
// })



const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
const upload = multer({ storage })

//////////////////////////////////////////////////////////////////////
///// search restaurant
//have both query
async function bothQuery(locationPara: any, dishPara: any) {
    let locationReq: any = locationPara
    let locationResult: String[] = locationReq.split(',')

    let dishReq: any = dishPara
    let dishResult: String[] = dishReq.split(',')

    let searchResultArray: any[] = [];
    let sql = format(`select distinct name, address, restaurants.id from restaurants 
                    inner join restaurant_dish 
                    on restaurants.id = restaurant_dish.restaurant_id 
                    inner join dishes 
                    on restaurant_dish.dish_id = dishes.id
                    where restaurants.district in (%L)
                    and dishes.dish in (%L)`, locationResult, dishResult)

    // console.log(sql)
    let testing = await client.query(sql)

    // console.log(testing.rows)
    // console.log(searchResultArray)
    searchResultArray.push(testing.rows)
    return searchResultArray
}

async function eitherQuery(reqPar: any) {
    let eitherReq: any = reqPar
    let eitherResult: String[] = eitherReq.split(',')

    let searchResultArray: any[] = [];
    let sql = format(`select distinct name, address, restaurants.id from restaurants 
                    full outer join restaurant_dish 
                    on restaurants.id = restaurant_dish.restaurant_id 
                    full outer join dishes 
                    on restaurant_dish.dish_id = dishes.id
                    where restaurants.district in (%L)
                    or dishes.dish in (%L)`, eitherResult, eitherResult)

    // console.log(sql)
    let testing = await client.query(sql)

    // for (let i = 0; i < eitherResult.length; i++) {
    //     let searchContent = await client.query(`select * from restaurants 
    //                                             where district = '${eitherResult[i]}' 
    //                                             or style like '%${eitherResult[i]}%'`)
    //     searchResultArray.push(searchContent.rows)
    // }
    searchResultArray.push(testing.rows)
    return searchResultArray
}

// search by territory
app.get('/searchByTerritory', async function (req, res) {
    let finalResult;
    if (req.query.location && req.query.dish) {
        let bothQueryResult = await bothQuery(req.query.location, req.query.dish)
        finalResult = bothQueryResult
    }
    else if (req.query.location || req.query.dish) {
        if (req.query.location) {
            let eitherQueryResult = await eitherQuery(req.query.location)
            finalResult = eitherQueryResult
        } else {
            let eitherQueryResult = await eitherQuery(req.query.dish)
            finalResult = eitherQueryResult
        }
    }

    // console.log(finalResult)
    // console.log("=======================")

    res.json(finalResult)
})

app.get('/searchByName/:name', async function (req, res) {
    let results = req.params
    // console.log(results)
    let searchResultArray;
    let searchContent = await client.query(`select * from restaurants 
                                            where name like'%${results.name}%'`)
    searchResultArray = searchContent.rows
    // console.log(searchResultArray)
    res.json(searchResultArray)
})

//////////// search end

////////////////////////SESSION/////////////////////////////////////////////////////////////

let sessionMiddleware = expressSession({
    secret: '123 pick a date',
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: Date.now() + (30 * 86400 * 1000)
    }
})

app.use(sessionMiddleware)

// app.use(expressSession({
//     secret: '123 pick a date',
//     resave: true,
//     saveUninitialized: true,
//     cookie: {
//         maxAge: Date.now() + (30 * 86400 * 1000)
//     }
// }))

app.use(express.static('sample'));
////////////////////////SESSION/////////////////////////////////////////////////////////////


/////////////////////////socketIO
const server = new http.Server(app);
const io = new SocketIO(server);

io.use(function(socket, next) {
    const request = socket.request as express.Request;
    sessionMiddleware(request, request.res as express.Response, next as express.NextFunction);
})

io.on('connection', async function (socket) {
    // socket.request.session.socketID = socket.id
    // socket.request.session.save();
    // console.log(`web socket connected, id = ${socket.id}`);
    console.log(socket.request.session['user'].id)

    let events = await client.query(`
    SELECT distinct event_id FROM participations
    where user_id = $1`,
    [socket.request.session['user'].id]);

    console.log("SOCKET EVENTS", events.rows)

    if(socket.request.session['user']){
        socket.join(`user-${socket.request.session['user'].id}`); 
        // console.log(`i join a room ${socket.request.session['user'].id}`) 
        // for (let event of events.rows){
        //     socket.join(`event-${event.event_id}`); 
        // }
    }
});
//////////////////////////





// vvvvvv changing main pages vvvvvvvv
function getPage(page: string) {
    app.get(`/${page}`, isLoggedIn, (req, res) => {
        res.sendFile(path.resolve(`protected/${page}.html`));
    })
}

getPage('calendar');
getPage('friends');
getPage('events');
getPage('search');
getPage('options')
// ^^^^^^^^ end changing main pages ^^^^^^^^^

// vvvvvvv login & reg vvvvvvvv

const SALT_ROUNDS = 10;

async function hashPassword(plainPassword: string) {
    const hash = await bcrypt.hash(plainPassword, SALT_ROUNDS);
    return hash;
}

async function checkPassword(plainPassword: string, hashPassword: string) {
    const match = await bcrypt.compare(plainPassword, hashPassword);
    return match;
}

app.post('/registration', upload.any(), async (req, res) => {

    let existingEmails = await client.query(`SELECT email FROM users
                                            WHERE email = $1`,
                                            [req.body.email]);
    console.log(existingEmails.rows[0]);
    if (existingEmails.rows[0]) {
        console.log("1");
        res.json(false);
    } else {
        console.log("New User:", req.body);
        const password = await hashPassword(req.body.password);
        await client.query(`INSERT INTO users 
                            (email, name, password, created_at, updated_at)
                            VALUES ($1, $2, $3, Now(), NOW())`,
                            [req.body.email, req.body.username, password]);
        console.log("2")
        res.json(true);
    }
})

app.post('/login', upload.any(), async (req, res) => {
    const user = (await client.query(`
        SELECT * FROM users
        WHERE users.email = $1`,
        [req.body.email])).rows[0];
    if (!user) {
        console.log("no user");
        return res.json({ id: false });
    }
    const match = await checkPassword(req.body.password, user.password);
    if (match) {
        console.log("password match")
        req.session['user'] = user;
        return res.json(user);
    } else {
        console.log("wrong password")
        return res.json({ id: false });
    }
})

function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session && req.session['user']) {
        next();
    } else {
        res.redirect('/');
    }
}

app.get('/sign-out', (req, res) => {
    console.log("signed out.");
    req.session['user'] = false;
    res.redirect('/')
})

app.get('/user-infos', isLoggedIn, async(req, res) => {
    const user = (await client.query(`
        SELECT * FROM users
        WHERE users.email = $1`,
        [req.session['user'].email])).rows[0];
    console.log(user);
    
    res.json(user);
})



// ^^^^^^ login & reg ^^^^^^^^

// vvvvvv events related vvvvvvv

app.get('/loadEvents', isLoggedIn, async(req, res)=>{
    let events = await client.query(`
    SELECT * FROM events 
    INNER JOIN participations
    on events.id = participations.event_id
    WHERE participations.user_id = $1
    ORDER BY events.created_at`,
    [req.session['user'].id]);

    res.json(events.rows)



})

app.get('/event', isLoggedIn, async (req, res) => { 
    res.sendFile(path.resolve(`protected/event.html`));
})

app.get('/event/:id', isLoggedIn, async (req, res) => {
    
    let event = await client.query(`SELECT * FROM events 
                                    WHERE id = $1`,
                                    [req.params.id]);
    res.json(event.rows[0]);
})


app.get('/latest-events', isLoggedIn, async(req, res)=>{
    let events = await client.query(`
    SELECT *
    FROM events
    INNER JOIN participations
    ON events.id = participations.event_id
    WHERE participations.user_id = $1
    ORDER BY events.created_at DESC
    LIMIT 3`,
    [req.session['user'].id]);

    res.json(events.rows)
})

// ^^^^^^ events related ^^^^^^^^


///////////////////////////////////////////// calendar create page 
app.get('/searchRestaurantCreate/:name', isLoggedIn, async function (req, res) {
    let results = req.params
    // console.log(results)
    let searchResultArray;
    let searchContent = await client.query(`select * from restaurants 
                                            where name like '%${results.name}%'`)
    searchResultArray = searchContent.rows
    // console.log(searchResultArray)
    res.json(searchResultArray)
})

app.get('/searchFdsCreate/:name', isLoggedIn, async function (req, res) {
    let results = req.params
    console.log(results)
    let searchResultArray;
    console.log(req.session['user'].id)
    let searchContent = await client.query(`select * from users 
                                            inner join friendship
                                            on users.id = friendship.adder_id
                                            or users.id = friendship.receiver_id
                                            where 
                                            users.id != ${req.session['user'].id}
                                            and (users.name like '%${results.name}%'
                                                or users.email like '%${results.name}%') 
                                            and (friendship.adder_id= ${req.session['user'].id}
                                                or friendship.receiver_id = ${req.session['user'].id})
                                            and friendship.status = 'accepted'`)
    // console.log(searchContent)                
    searchResultArray = searchContent.rows
    console.log("search fd from fd list")
    console.log(searchContent.rows)
    res.json(searchResultArray)
})

app.post('/createEvent', isLoggedIn, upload.any(), async function(req, res) {
    // console.log(req.body)
    // console.log(req.session['user'])

    let usersArr = req.body.confirmedFds.split(',')
    let confirmedRestArr = req.body.confirmedRestaurants.split(',')
    console.log(usersArr)
    console.log(confirmedRestArr)
    let eventID = await client.query(`insert into events (name, creator_id) 
                                      values ($1, $2)
                                      returning id`, 
                                      [req.body.eventName, req.session['user'].id])

    // console.log(eventID)
    // console.log(usersArr)
    // console.log(confirmedRestArr)
    
    console.log("inserting creator to participation")
    await client.query(`insert into participations (user_id, event_id) 
                        values ($1, $2)`, 
                        [req.session['user'].id, eventID.rows[0].id])
    console.log("inserted")

    console.log("inserting participators to participation")   
    for (let eachUser of usersArr) {
        await client.query(`insert into participations (user_id, event_id) 
                            values ($1, $2)`, 
                            [eachUser, eventID.rows[0].id])
    }
    console.log("inserted")

    console.log("inserting restaurants to event_venues")  
    for (let eachRestaurant of confirmedRestArr) {
        await client.query(`insert into event_venues (restaurant_id, event_id) 
                            values ($1, $2)`, 
                            [eachRestaurant, eventID.rows[0].id])
    }
    console.log("inserted")

    // let creatorID = await client.query(`select id from users where name = ${req.session['user']}`);
    res.json(eventID.rows[0].id)
} )
/////////////////////////

////////////////////////////////////////////// friends list
// search users
app.get('/searchUsers/:name', isLoggedIn, async function (req, res) {
    let results = req.params
    let searchResultIDs = [];
    let searchContents = await client.query(`select * from users
                                            where id != ${req.session['user'].id}
                                            and (name like '%${results.name}%'
                                            or email like '%${results.name}')`)
    // console.log(searchContents.rows[0])
    for (let searchContent of searchContents.rows) {
        searchResultIDs.push(searchContent.id)
    }
    // console.log(searchResultIDs)

    let relation =  {}
    let friends: Array<any> = []
    let strangers: Array<any> = []
    
    for (let searchResultID of searchResultIDs) {
        let checkExistingFD: QueryResult = await client.query(`select * from friendship
                                                where (${searchResultID} = adder_id 
                                                and ${req.session['user'].id} = receiver_id
                                                and status = 'accepted')
                                                or (${req.session['user'].id} = adder_id
                                                and ${searchResultID} = receiver_id
                                                and status = 'accepted')`)
        // console.log(checkExistingFD.rows)
        if (checkExistingFD.rows[0]) {
            if (checkExistingFD.rows[0].receiver_id = searchResultID) {
                friends.push(checkExistingFD.rows[0].receiver_id)
            } else if (checkExistingFD.rows[0].adder_id = searchResultID) {
                friends.push(checkExistingFD.rows[0].adder_id)
            }
        } else {
            strangers.push(searchResultID)
        }
    }

    relation['friends'] = friends
    relation['strangers'] = strangers
    // console.log(relation)

    let resObj =  {}
    let resFriends: Array<any> = []
    let resStrangers: Array<any> = []
    
    for (let friend of relation['friends']) {
        let friendName = await client.query(`select * from users
                                            where users.id = ${friend}`)
        resFriends.push(friendName.rows) 
    }
    // console.log(resFriends)
    for (let stranger of relation['strangers']) {
        let strangerName = await client.query(`select * from users
                                            where users.id = ${stranger}`)
        resStrangers.push(strangerName.rows)   
    }

    resObj['resFriends'] = resFriends
    resObj['resStrangers'] = resStrangers

    // console.log(resObj)
    res.json(resObj)
})


// send friends request
app.post('/addFds', isLoggedIn, upload.any(), async function (req, res) {
    console.log(`this is add fd function and the user is ${req.session['user'].id}`)
    console.log(`this user want to add user ID: ${req.body.wantToAdd}`)

    console.log("inserting fdRequest")
    await client.query(`insert into friendship 
                        (adder_id, receiver_id, status)
                        values ($1, $2, $3) returning id`,
                        [req.session['user'].id, 
                        req.body.wantToAdd, 
                        'pending'])
    console.log("inserted")
    res.json("request sent")
})


// check friends request
app.get('/checkFdRequest', isLoggedIn, async function (req, res) {
    //console.log(`this is check fd req function, and the user is ${req.session['user'].id}`)
    let checkAddersID: QueryResult = await client.query(`select adder_id from friendship
                                                 where receiver_id = ${req.session['user'].id}
                                                 and status = 'pending'`)
    // console.log(checkAddersID.rows)
    let adderNamesResult = [];
    for (let checkAdderID of checkAddersID.rows) {
        // console.log(checkAdderID)
        let adderNames = await client.query(`select * from users
                                             where id = ${checkAdderID.adder_id}`)
        adderNamesResult.push(adderNames.rows[0])
        // console.log(adderNames.rows[0])
    }
    // console.log(adderNamesResult)
    res.json(adderNamesResult)

})

// accept or decline friends request
app.get('/acceptFdReq/:id', isLoggedIn, async function (req, res) {
    let result = req.params
    console.log(result)
    console.log("accept fd req and updating to database")
    await client.query(`update friendship set status = 'accepted'
                        where adder_id = ${result.id}
                        and receiver_id = ${req.session['user'].id}`)      
    console.log("finished")      
    res.send("finished")
})

app.get('/declineFdReq/:id', isLoggedIn, async function (req, res) {
    let result = req.params
    console.log(result)
    console.log("decline fd req and updating to database")
    await client.query(`update friendship set status = 'declined'
                        where adder_id = ${result.id}
                        and receiver_id = ${req.session['user'].id}`)
    console.log("finished")
    res.send("finished")
})

//show accepted fds 
app.get('/showAcceptedFds', isLoggedIn, async function (req, res) {
    // console.log(`this is show fd function, and the user is ${req.session['user'].id}`)

    let resObj =  {}
    let resYouAcc: Array<any> = []
    let resYouReq: Array<any> = []
    
    let IDsYouAccepted = await client.query(`select adder_id from friendship
                                            where receiver_id = ${req.session['user'].id}
                                            and status = 'accepted'`)
    // console.log(`this is B:`)
    // console.log(IDsYouAccepted.rows)
    let IDsAcceptedArr = []
    for (let IDYouAccepted of IDsYouAccepted.rows) {
        console.log(`this is C:`)
        console.log(IDYouAccepted)
        IDsAcceptedArr.push(IDYouAccepted.adder_id)
    } 
    // console.log('this is ids arr')
    // console.log(IDsAcceptedArr)

    for (let IDYouAdd of IDsAcceptedArr) {
        let resultOfAdd = await client.query(`select id, name, profile_picture from users
                                            where id = ${IDYouAdd}`)
        resYouAcc.push(resultOfAdd)
    }


    let IDsYouRequested = await client.query(`select receiver_id from friendship
                                            where adder_id = ${req.session['user'].id}
                                            and status = 'accepted'`)
    // console.log(`this is D:`)
    // console.log(IDsYouRequested.rows)
    let IDsRequestedArr = []
    for (let IDYouRequested of IDsYouRequested.rows) {
        // console.log(`this is E:`)
        // console.log(IDYouRequested)
        IDsRequestedArr.push(IDYouRequested.receiver_id)
    } 
    // console.log('this is ids arr')
    // console.log(IDsRequestedArr)

    for (let IDYouReq of IDsRequestedArr) {
        let resultOfRequest = await client.query(`select id, name, profile_picture from users
                                                where id = ${IDYouReq}`)
        resYouReq.push(resultOfRequest)
    }

    resObj['resYouAcc'] = resYouAcc
    resObj['resYouReq'] = resYouReq

    console.log("sending all your fds to browser")
    res.json(resObj)
})

// remove accepted fds
app.get('/unfriend/:id', isLoggedIn, async function(req, res) {
    console.log(`you are unfriending ${req.params.id}`)
    await client.query(`delete from friendship
                        where 
                        (adder_id = ${req.params.id}
                        and receiver_id = ${req.session['user'].id})
                        or 
                        (adder_id = ${req.session['user'].id}
                        and receiver_id = ${req.params.id})`)
    res.json("removed")
})

// pm chat 
app.get('/pmChat', isLoggedIn, async function(req, res) {
    res.sendFile(path.resolve(`protected/privateChat.html`));
})

app.get('/loadFdsPage', isLoggedIn, async function(req, res) {
    res.sendFile(path.resolve(`protected/friends.html`));
})

app.post('/sdPM/:fdID', isLoggedIn, upload.any(), async function(req, res) {
    console.log(req.body.msg)
    console.log(req.params)
    await client.query(`insert into private_message
                        (sender_id, content, receiver_id, created_at)
                        values
                        ($1, $2, $3, NOW())`,
                        [req.session['user'].id, req.body.msg, req.params.fdID])
    // console.log(insertedPM)
    if(req.session){
        console.log(`i send to this room ${req.params.fdID}`)
        io.to(`user-${req.params.fdID}`).emit(`msg`, {
            content: req.body.msg,
            type: "pm",
            friendID: req.session['user'].id,
        });
    }
    res.json("received msg")
})






app.get('/loadPM/:fdID', isLoggedIn, async function(req, res) {
    // console.log(`this is load pm function, you are loading msg of`)
    // console.log(req.params)
    let loadMsgs = await client.query(`select private_message.created_at, private_message.content, private_message.sender_id
                                    from private_message
                                    inner join users
                                    on sender_id = users.id
                                    where (sender_id = $1
                                    and receiver_id = $2)
                                    or (receiver_id = $1
                                    and sender_id = $2)`, 
                                    [req.session['user'].id,
                                    req.params.fdID])
    for (let loadMsg of loadMsgs.rows) {
        
        if (loadMsg.sender_id == req.session['user'].id) {
            // console.log(loadMsg)
            loadMsg.sender = 'you'
        }
    }
    res.json(loadMsgs)
})


////////////////////////

// vvvvvvv Vote Date related vvvvvvvvv

app.post('/vote/:date', isLoggedIn, upload.any(), async (req, res)=>{
    console.log("----------------------")
    console.log("body: ", req.body);
    console.log("voted date: ", req.params.date);

    let date = req.params.date;
    let eventDateID = (await client.query(`
    SELECT * FROM event_dates
    WHERE event_id = $1
    AND date = $2`,
    [req.body.eventID, date]));

    console.log(eventDateID.rows[0]);

    if (eventDateID.rowCount == 0){
        console.log(("new vote"));
        
        eventDateID = await client.query(`
        INSERT INTO event_dates
        (date, event_id)
        VALUES
        ($1, $2)
        RETURNING id`,
        [date, req.body.eventID]);
        console.log(eventDateID.rows[0].id)

        await client.query(`
        INSERT INTO event_date_votes
        (user_id, event_date_id, availability, updated_at)
        VALUES
        ($1, $2, 'TRUE', NOW())`,
        [req.session['user'].id, eventDateID.rows[0].id])
    } else {
        console.log("oldID: ", eventDateID.rows[0].id)
        console.log("userID: ", req.session['user'].id)
        console.log("eventDateID: ", eventDateID.rows[0].id);

        let userVote = await client.query(`
        SELECT * FROM event_date_votes
        INNER JOIN event_dates
        ON event_date_votes.event_date_id = event_dates.id
        WHERE event_dates.date = $1
        AND event_date_votes.user_id = $2`,
        [date, req.session['user'].id])

        if (userVote.rowCount == 0){
            await client.query(`
            INSERT INTO event_date_votes
            (user_id, event_date_id, availability, updated_at)
            VALUES
            ($1, $2, 'TRUE', NOW())`,
            [req.session['user'].id, eventDateID.rows[0].id])
        } else {
            await client.query(`
            UPDATE event_date_votes SET availability = 'TRUE'
            WHERE user_id = $1 AND event_date_id = $2`,
            [req.session['user'].id, eventDateID.rows[0].id])
        }
    }
    res.send("ok")
})

app.post('/voteAgainst/:date', isLoggedIn, upload.any(), async (req, res)=>{
    console.log("----------------------")
    console.log("body: ", req.body);
    console.log("voted date: ", req.params.date);

    let date = req.params.date;
    let eventDateID = (await client.query(`
    SELECT * FROM event_dates
    WHERE event_id = $1
    AND date = $2`,
    [req.body.eventID, date]));

    console.log(eventDateID.rows[0]);

    if (eventDateID.rowCount == 0){
        console.log(("new vote"));
        
        eventDateID = await client.query(`
        INSERT INTO event_dates
        (date, event_id)
        VALUES
        ($1, $2)
        RETURNING id`,
        [date, req.body.eventID]);
        console.log(eventDateID.rows[0].id)

        await client.query(`
        INSERT INTO event_date_votes
        (user_id, event_date_id, availability, updated_at)
        VALUES
        ($1, $2, 'FALSE', NOW())`,
        [req.session['user'].id, eventDateID.rows[0].id])
    } else {
        console.log("oldID: ", eventDateID.rows[0].id)
        console.log("userID: ", req.session['user'].id)
        console.log("eventDateID: ", eventDateID.rows[0].id);

        let userVote = await client.query(`
        SELECT * FROM event_date_votes
        INNER JOIN event_dates
        ON event_date_votes.event_date_id = event_dates.id
        WHERE event_dates.date = $1
        AND event_date_votes.user_id = $2`,
        [date, req.session['user'].id])

        if (userVote.rowCount == 0){
            await client.query(`
            INSERT INTO event_date_votes
            (user_id, event_date_id, availability, updated_at)
            VALUES
            ($1, $2, 'FALSE', NOW())`,
            [req.session['user'].id, eventDateID.rows[0].id])
        } else {
            await client.query(`
            UPDATE event_date_votes SET availability = 'FALSE'
            WHERE user_id = $1 AND event_date_id = $2`,
            [req.session['user'].id, eventDateID.rows[0].id])
        }
    }
    res.send("something")
})

app.get('/drawDateSkills/:id', isLoggedIn, async(req, res)=>{
    let votedDates = await client.query(`
    SELECT event_dates.date, event_date_votes.user_id, event_date_votes.availability
    FROM event_dates
    INNER JOIN event_date_votes
    ON event_dates.id = event_date_votes.event_date_id
    WHERE event_dates.event_id = $1
    ORDER BY event_dates.date`,
    [req.params.id]);
    res.json(votedDates.rows)
})

app.get('/countEventPcs/:id', isLoggedIn, async(req, res)=>{
    let numberOfPcs = await client.query(`
    SELECT * FROM events
    INNER JOIN participations
    ON events.id = participations.event_id
    WHERE events.id = $1`,
    [req.params.id]);
    res.json(numberOfPcs.rowCount);
})

// ^^^^^^^^ Vote Date related ^^^^^^^^^

// vvvvvvvv Vote Venue related vvvvvvvv

app.get('/drawVenueSkills/:id', isLoggedIn, async(req, res)=>{
    let votedVenues = await client.query(`
    SELECT event_venues.id, restaurants.name, restaurants.address, event_venue_votes.user_id, event_venue_votes.favoured
    FROM event_venues
    FULL JOIN restaurants
    ON event_venues.restaurant_id = restaurants.id
    FULL JOIN event_venue_votes
    ON event_venues.id = event_venue_votes.event_venue_id
    WHERE event_venues.event_id = $1
    ORDER BY restaurants.id`,
    [req.params.id]);
    res.json(votedVenues.rows)
})

function voteVenues(forOrAgainst: "For"|"Against"){
    let boolean: boolean;
    if (forOrAgainst == "For"){
        boolean = true;
    } else {
        boolean = false;
    }

    app.post(`/vote${forOrAgainst}Venue/:id`, isLoggedIn, upload.any(), async(req, res)=>{
        let eventID = req.body.eventID;
        let userID = req.session['user'].id;
        let venueID = req.params.id;
        console.log(boolean);
    
        let voted = await client.query(`
        SELECT * FROM event_venue_votes
        INNER JOIN event_venues
        ON event_venue_votes.event_venue_id = event_venues.id
        WHERE event_venue_votes.user_id = $1
        AND event_venues.id = $2
        AND event_venues.event_id = $3`,
        [userID, venueID, eventID]);
    
        if (voted.rowCount != 0){
            await client.query(`
            UPDATE event_venue_votes
            SET favoured = '${boolean}',
                updated_at = NOW()
            WHERE user_id = $1
            AND event_venue_id = $2`,
            [userID, venueID])
        } else {
            await client.query(`
            INSERT INTO event_venue_votes
            (user_id, event_venue_id, favoured, updated_at)
            VALUES ($1, $2, '${boolean}', NOW())`,
            [userID, venueID])
        }
    
        res.json("ok")
    })
}

voteVenues("For");
voteVenues("Against");

app.post("/addVenue/:eventID/:venueID", isLoggedIn, async(req, res)=>{
    console.log(req.params.eventID, req.params.venueID);
    let result = await client.query(`
    SELECT * FROM event_venues
    WHERE event_id = $1
    AND restaurant_id = $2`,
    [req.params.eventID, req.params.venueID]);

    if (result.rowCount == 0){
        await client.query(`
        INSERT INTO event_venues
        (event_id, restaurant_id, updated_at)
        VALUES
        ($1, $2, NOW())`,
        [req.params.eventID, req.params.venueID])
        res.json({result: true})
    } else {
        res.json({result: false})
    }
})




// ^^^^^^^^ Vote Venue related ^^^^^^^^^^

// vvvvvvvv chat related vvvvvvvvv

app.get('/message/:eventID', isLoggedIn, async(req, res)=>{
    let messages = await client.query(`
    SELECT messages.content, messages.id, messages.user_id, users.email, users.name, messages.created_at
    FROM messages
    INNER JOIN users
    ON messages.user_id = users.id
    WHERE messages.event_id = $1
    ORDER BY messages.created_at DESC`,
    [req.params.eventID]);

    for (let msg of messages.rows) {
        if (msg.user_id == req.session['user'].id) {
            // console.log(msg)
            msg.sender = 'you'
        }
    }

    console.log(messages.rows);
    res.json(messages.rows);
})

app.get('/latest-message/:eventID', isLoggedIn, async(req, res)=>{
    let messages = await client.query(`
    SELECT messages.content, messages.id, users.email, users.name, messages.created_at
    FROM messages
    INNER JOIN users
    ON messages.user_id = users.id
    WHERE messages.event_id = $1
    ORDER BY messages.created_at DESC
    LIMIT 1`,
    [req.params.eventID]);
    console.log(messages.rows);
    if (messages.rowCount == 0){
        res.json({})
    } else {
        res.json(messages.rows[0]);
    }
})


app.post('/message/:eventID', isLoggedIn, upload.any(), async(req, res)=>{
    console.log(req.body.message);
    await client.query(`
    INSERT INTO messages
    (user_id, content, event_id, updated_at)
    VALUES ($1, $2, $3, NOW())`,
    [req.session['user'].id, req.body.message, req.params.eventID])

    console.log(`i send to this event${req.params.eventID}`)

    let participants = await client.query(`
    SELECT user_id
    FROM participations
    WHERE event_id = $1`, [req.params.eventID]);
    
    for(let participant of participants.rows){
        io.to(`user-${participant.user_id}`).emit(`msg`, {
            content: req.body.message,
            type: "event",
            eventID: req.params.eventID,
        });
    }

    res.json("ok")
})

// ^^^^^^^^ chat related ^^^^^^^^^

// vvvvvvv personal calendar vvvvvvv
app.put('/personalSchedule/:id', isLoggedIn, upload.any(), async(req, res)=>{
    console.log(req.body);
    console.log(req.params.id);

    let result = await client.query(`
    SELECT * FROM personal_schedule
    WHERE user_id = $1
    AND date = $2`,
    [req.params.id, req.body.date]);
    
    if (result.rowCount == 0){
        await client.query(`
        INSERT INTO personal_schedule
        (user_id, date, content, updated_at)
        VALUES
        ($1, $2, $3, NOW())`,
        [req.params.id, req.body.date, req.body.content]);
    } else if (req.body.content == ''){
        await client.query(`
        DELETE FROM personal_schedule
        WHERE user_id = $1
        AND date = $2`,
        [req.params.id, req.body.date])
    } else {
        await client.query(`
        UPDATE personal_schedule
        SET (content, updated_at) = ($1, NOW())
        WHERE user_id = $2
        AND date = $3`,
        [req.body.content, req.params.id, req.body.date]);
    }
    
    res.send("ok")
})

app.get('/personalSchedule/:date', isLoggedIn, async(req, res)=>{
    let content = await client.query(`
    SELECT content
    FROM personal_schedule
    WHERE user_id = $1
    AND date = $2`,
    [req.session['user'].id, req.params.date]);

    if (content.rowCount != 0){
        res.json(content.rows[0])
    } else {
        res.json({content: ""})
    }
})

app.get('/scheduledDates', isLoggedIn, async(req, res)=>{
    let dates = await client.query(`
    SELECT date
    FROM personal_schedule
    WHERE user_id = $1`,
    [req.session['user'].id]);

    res.json(dates.rows)
})


// ^^^^^^^^ personal calendar ^^^^^^^^^



// vvvvvvvv user info related vvvvvvvvv

app.post('/upload-profile-pic', isLoggedIn, upload.single('profile-pic'), async(req, res)=>{
    console.log(req.file.filename);
    console.log(req.body.userID);
    if (!req.body.userID){
        return res.send(req.file.filename);
    } else {
        await client.query(`
        UPDATE users
        SET (profile_picture, updated_at) = ($1, NOW())
        WHERE id = $2`,
        [req.file.filename, req.body.userID]);
        return res.send(req.file.filename);
    }
})

app.get('/eventFdList/:eventID', isLoggedIn, async(req, res)=>{
    let fdList = await client.query(`
    SELECT * FROM users
    INNER JOIN participations
    ON users.id = participations.user_id
    WHERE participations.event_id = $1`,
    [req.params.eventID]);

    res.json(fdList.rows);
})

// ^^^^^^^^ user info related ^^^^^^^^

// vvvvvvvvvvv event page related vvvvvvvvvvv
app.post('/upload-event-pic', isLoggedIn, upload.single('event-pic'), async(req, res)=>{
    console.log(req.file.filename);
    console.log(req.body.eventID);
    if (!req.body.eventID){
        return res.send(req.file.filename);
    } else {
        await client.query(`
        UPDATE events
        SET (picture, updated_at) = ($1, NOW())
        WHERE id = $2`,
        [req.file.filename, req.body.eventID]);
        return res.send(req.file.filename);
    }
})

app.get('/event-pic/:eventID', isLoggedIn, async(req, res)=>{
    let picture = await client.query(`
    SELECT picture
    FROM events
    WHERE id = $1`,
    [req.params.eventID])
    res.json(picture.rows[0]);
})

app.post('/addFriendToEvent/:userID/:eventID', isLoggedIn, async(req, res)=>{
    console.log(req.params.userID, req.params.eventID);
    let checkParticipation = await client.query(`
    SELECT * FROM participations
    WHERE user_id = $1
    AND event_id = $2`,
    [req.params.userID, req.params.eventID]);

    if (checkParticipation.rowCount == 0){
        await client.query(`
        INSERT INTO participations
        (user_id, event_id, updated_at)
        VALUES
        ($1, $2, NOW())`,
        [req.params.userID, req.params.eventID]);
        res.json({result: true});
    } else {
        res.json({result: false});
    }
})







// ^^^^^^^^^ event page related ^^^^^^^^^^



app.use(express.static('public'));
app.use(express.static('uploads'));
app.use(isLoggedIn, express.static('protected'));

app.use((req, res, next) => {
    if (req.method !== 'GET') {
        next;
        return;
    }
    res.send("404 Not Found.")
})




/////////////////////////socket server
const PORT = 8500;
server.listen(PORT, () => {
    console.log('listening on http://127.0.0.1:' + PORT);
})
//////////////////////// express server
// const PORT = 8080;
// app.listen(PORT, () => {
//     console.log('listening on http://127.0.0.1:' + PORT);
// })