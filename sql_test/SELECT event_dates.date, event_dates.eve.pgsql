SELECT event_dates.date, event_dates.event_id, event_date_votes.user_id, event_date_votes.availability
FROM event_dates
INNER JOIN event_date_votes
ON event_dates.id = event_date_votes.event_date_id
ORDER BY event_dates.date;