const main = document.querySelector('#main');

function switchLoginCreate(){
    if(main.querySelector("form").id == "login"){
        main.innerHTML = `
        <form id="registration">
            <div class="switch-login-create-user" onclick="switchLoginCreate()">Existing User Login</div>
            <input name="email" type="email" class="input-box" placeholder="Email address">
            <input name="username" type="text" class="input-box" placeholder="Display name">
            <input name="password" type="password" class="input-box" placeholder="Password">
            <input name="passwordCheck" type="password" class="input-box" placeholder="Password check">
            <div class="error"></div>
            <div value="Sign Up" class="btn" onclick="createUser()">Sign-up</div>
        </form>
        `;
    } else {
        main.innerHTML =`
        <form id="login">
            <div class="switch-login-create-user" onclick="switchLoginCreate()">Create Account Now</div>
            <input name="email" type="email" class="input-box" placeholder="Login email">
            <input name="password" type="password" class="input-box" placeholder="Password">
            <div class="error"></div>
            <div value="Login" class="btn" onclick="login()">Login</div>
        </form>
        `;       
    }
}

async function createUser(){
    let form = document.querySelector(`#registration`);
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!form.email.value|| !form.username.value||
        !form.password.value|| !form.passwordCheck.value){
        form.querySelector('.error').innerText = "Please fill in all boxes."
    } else if (!re.test(form.email.value) && form.password.value !== form.passwordCheck.value){
        form.email.style.border = '2px solid #E14742';
        form.querySelector('.error').innerText = "Please re-enter email and password.";
        form.password.style.border = '2px solid #E14742';
        form.passwordCheck.style.border = '2px solid #E14742';
        form.password.value = null;
        form.passwordCheck.value = null;
    }else if (re.test(form.email.value) && form.password.value !== form.passwordCheck.value){
        form.email.style.border = 'none';
        form.password.style.border = '2px solid #E14742';
        form.passwordCheck.style.border = '2px solid #E14742';
        form.password.value = null;
        form.passwordCheck.value = null;
        form.querySelector('.error').innerText = "Please check and re-enter password.";
    } else if (!re.test(form.email.value) && form.password.value == form.passwordCheck.value) {
        form.email.style.border = '2px solid #E14742';
        form.password.style.border = 'none';
        form.passwordCheck.style.border = 'none';
        form.querySelector('.error').innerText = "Please check and re-enter email.";
    } else {
        let formData = new FormData();
        formData.append('email', form.email.value);
        formData.append('username', form.username.value);
        formData.append('password', form.password.value);
        console.log(formData);
        const res = await fetch(`/registration`,{
            method: "POST",
            body: formData,
        })
        let json = await res.json();
        console.log(json);
        if (!json){
            form.email.style.border = '2px solid #E14742';
            form.querySelector('.error').innerText = "Existing user email, please login.";
            return;
        } else {
            let loginFormData = new FormData();
            loginFormData.append('email', form.email.value);
            loginFormData.append('password', form.password.value);
            await fetch(`/login`, {
                method: "POST",
                body: loginFormData,
            })
            window.location.href = "/main.html";
        }

    }
}

async function login(){
    let form = document.querySelector(`#login`);
    let formData = new FormData();
    formData.append('email', form.email.value);
    formData.append('password', form.password.value);
    const res = await fetch(`/login`, {
        method: "POST",
        body: formData,
    })
    let json = await res.json();
    if (!json.id){
        form.password.value = null;
        form.querySelector('.error').innerText = "Invalid email or password.";
    } else {
        
        // console.log("loggedIn");
        // let res = await fetch('/main.html');
        // let text = res.text();
        window.location.href = "/main.html";
    }
}

document.addEventListener('keypress', function(e){
    if (e.key === 'Enter'){
        if(main.querySelector("form").id == "login"){
            login();
        } else {
            createUser();
        }
    }
})