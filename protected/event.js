function addListenerToEventDates(){
    let dates = document.querySelectorAll(".date");
    let today = new Date();
    let yesterday = today.setDate(today.getDate() - 1);
    for (let date of dates){
        let thisDate = new Date(date.id.slice(4, 14));
        if (thisDate >= yesterday){
            date.querySelector("span").setAttribute("onclick", `voteDate("${date.id}")`);
        }
    }
}

// date_id = date.id.slice(4)

function voteDate(dateStr){
    let showVotePanel = false;
    let chosenDate = document.querySelector(`#${dateStr}`);
    if(!chosenDate.classList.contains("voting")){
        showVotePanel = true;
    }
    loadCalendarOnDate(dateStr.slice(4));
    addListenerToEventDates()
    if (showVotePanel){
        console.log("print");
        chosenDate = document.querySelector(`#${dateStr}`);
        chosenDate.innerHTML += `
        <div class="voting-panel">
            <i class="fas fa-thumbs-up" onclick="voteFor('${dateStr}')"></i>
            <i class="fas fa-thumbs-down" onclick="voteAgainst('${dateStr}')"></i>
        </div>`
        chosenDate.classList.add("voting")
    }
}

async function voteFor(dateStr){
    console.log("Voted for", dateStr);
    let date = dateStr.slice(4);
    loadCalendarOnDate(date);
    addListenerToEventDates();
    let eventID = document.querySelector("#event-page").getAttribute('data-id');
    let formData = new FormData();
    formData.append("eventID", eventID);
    let res = await fetch(`/vote/${date}`, {
        method: 'POST',
        body: formData
    })
    console.log(await res.text());
    drawDateSkills(eventID);
}

async function voteAgainst(dateStr){
    console.log("Voted against", dateStr);
    let date = dateStr.slice(4);
    loadCalendarOnDate(date);
    addListenerToEventDates();
    let eventID = document.querySelector("#event-page").getAttribute('data-id');
    let formData = new FormData();
    formData.append("eventID", eventID);
    let res = await fetch(`/voteAgainst/${date}`, {
        method: 'POST',
        body: formData
    })
    console.log(await res.text());
    drawDateSkills(eventID);
}

async function drawDateSkills(eventID){
    let res = await fetch(`/drawDateSkills/${eventID}`);
    let votes = await res.json();
    let pcsRes = await fetch(`/countEventPcs/${eventID}`);
    let pcs = await pcsRes.json();
    let userID = document.querySelector("#profile-pic").getAttribute("data-id");
    
    let voteResultDiv = document.querySelector("#vote-date-results");
    let voteObjs = {};
    voteResultDiv.innerHTML = ""
    
    for (let vote of votes){
        if (!voteObjs[vote.date.slice(0, 10)]) {
            voteObjs[vote.date.slice(0,10)] = {
                votes: 0,
                votesFor: 0,
                votesAgainst: 0,
                thisUserVoted : "",
            };
        }
        if (vote.availability){
            voteObjs[vote.date.slice(0, 10)]['votes'] += 1;
            voteObjs[vote.date.slice(0, 10)]['votesFor'] += 1;
            if(vote.user_id == userID){
                voteObjs[vote.date.slice(0, 10)]['thisUserVoted'] = "available";
            }
        } else {
            voteObjs[vote.date.slice(0, 10)]['votes'] += 1;
            voteObjs[vote.date.slice(0, 10)]['votesAgainst'] += 1;
            if(vote.user_id == userID){
                voteObjs[vote.date.slice(0, 10)]['thisUserVoted'] = "unavailable";
            }
        }
    }
    for (let date in voteObjs){
        let voteObj = voteObjs[date];
        let percentageFor = Math.round(voteObj['votesFor'] / pcs * 100);
        let percentageAgainst = Math.round(voteObj['votesAgainst'] / pcs * 100);
        let thisDate = new Date(date);
        let today = new Date();
        let yesterday = today.setDate(today.getDate() - 1);

        if (document.querySelector(`#date${date}`)){
            document.querySelector(`#date${date}`).classList.add("voted");
        }

        voteResultDiv.innerHTML += `
        <div class="votes-container" data-id="${date}">
            <div class="skills-date">${date}</div>
            <div class="skills-container"></div>
        </div>`
        let skillFor = drawSkills(date, percentageFor, "for");
        let skillAgainst = drawSkills(date, percentageAgainst, "against");

        voteResultDiv.querySelector(`.votes-container[data-id="${date}"]`)
            .querySelector(".skills-container").appendChild(skillFor);
        voteResultDiv.querySelector(`.votes-container[data-id="${date}"]`)
            .querySelector(".skills-container").appendChild(skillAgainst);

        if (voteObj['thisUserVoted'] == "available"){
            voteResultDiv.querySelector(`.votes-container[data-id="${date}"]`)
                .querySelector(".skills-date").innerHTML += ` <i class="fas fa-vote-yea" style="color: #43cdd5"></i>`
        } else if (voteObj['thisUserVoted'] == "unavailable"){
            voteResultDiv.querySelector(`.votes-container[data-id="${date}"]`)
                .querySelector(".skills-date").innerHTML += ` <i class="fas fa-vote-yea" style="color: var(--below-50)"></i>`
        }

        if (thisDate <= yesterday){
            voteResultDiv.querySelector(`.votes-container[data-id="${date}"]`)
                .querySelector(".skills-date").style.textDecoration = "line-through";
        }
    }
}

function drawSkills(date, percentage, forOrAgainst){
    let skill = document.createElement('div');
    skill.innerHTML = `${percentage}%`;
    skill.classList.add("skills")
    skill.classList.add(forOrAgainst)
    skill.id = `skill${date}`;
    skill.style.width = `${percentage}%`;
    // if (percentage >= 90){
    //     skill.classList.add("over-90");
    // } else if (percentage >= 70){
    //     skill.classList.add("over-70");
    // } else if (percentage >= 50){
    //     skill.classList.add("over-50");
    // } 
    if (percentage == 0){
        skill.innerHTML = ""
    }
    if (percentage == 100){
        skill.style.borderRadius = "10px"
    }
    return skill;
}

function triggerEventBar(){
    let eventBar = document.querySelector("#event-title-bar");
    if(eventBar.classList.contains("active")){
        eventBar.classList.remove("active");
    } else {
        eventBar.classList.add("active");
    }
}

async function drawVenueSkills(eventID){
    let res = await fetch(`/drawVenueSkills/${eventID}`);
    let votes = await res.json();
    let pcsRes = await fetch(`/countEventPcs/${eventID}`);
    let pcs = await pcsRes.json();
    let voteResultDiv = document.querySelector("#vote-venue-results");
    let userID = document.querySelector("#profile-pic").getAttribute("data-id");
    voteResultDiv.innerHTML = "";
    let voteObjs = {};

    console.log(votes)

    for (let vote of votes){
        if (!voteObjs[vote.id]) {
            voteObjs[vote.id] = {
                name: vote.name,
                address: vote.address,
                votes: 0,
                votesFor: 0,
                votesAgainst: 0,
                thisUserVoted: "",
            };
        }
        if (vote.favoured == null){

        } else if (vote.favoured){
            voteObjs[vote.id]['votes'] += 1;
            voteObjs[vote.id]['votesFor'] += 1;
            if(vote.user_id == userID){
                voteObjs[vote.id]['thisUserVoted'] = "yes";
            }
        } else if (!vote.favoured){
            voteObjs[vote.id]['votes'] += 1;
            voteObjs[vote.id]['votesAgainst'] += 1;
            if(vote.user_id == userID){
                voteObjs[vote.id]['thisUserVoted'] = "no";
            }
        }
    }
    console.log(voteObjs);

    for (let venueID in voteObjs){
        let voteObj = voteObjs[venueID];
        let percentageFor = Math.round(voteObj['votesFor'] / pcs * 100);
        let percentageAgainst = Math.round(voteObj['votesAgainst'] / pcs * 100);
        voteResultDiv.innerHTML += `
        <div class="venue-votes-container">
            <i class="fas fa-thumbs-up venue" onclick="voteForVenue(${venueID})"></i>
            <div class="venue-vote" data-id="${venueID}">
                <div class="venue-container">
                    <div class="venue-name">${voteObjs[venueID].name}</div>
                    <div class="venue-address">${voteObjs[venueID].address}</div>
                </div>
                <div class="skills-container">
                </div>
            </div>
            <i class="fas fa-thumbs-down venue" onclick="voteAgainstVenue(${venueID})"></i>
        </div>`
        let skillFor = drawSkills(venueID, percentageFor, "for");
        let skillAgainst = drawSkills(venueID, percentageAgainst, "against");

        voteResultDiv.querySelector(`.venue-vote[data-id="${venueID}"]`)
            .querySelector(".skills-container").appendChild(skillFor);
        voteResultDiv.querySelector(`.venue-vote[data-id="${venueID}"]`)
            .querySelector(".skills-container").appendChild(skillAgainst);

            if (voteObj['thisUserVoted'] == "yes"){
                voteResultDiv.querySelector(`.venue-vote[data-id="${venueID}"] > .venue-container > .venue-name`).innerHTML += ` <i class="fas fa-vote-yea" style="color: #43cdd5"></i>`
            } else if (voteObj['thisUserVoted'] == "no"){
                voteResultDiv.querySelector(`.venue-vote[data-id="${venueID}"] > .venue-container > .venue-name`).innerHTML += ` <i class="fas fa-vote-yea" style="color: var(--below-50)"></i>`
            }
    }
}

async function voteForVenue(venueID){
    let eventID = document.querySelector("#event-page").getAttribute('data-id');
    let formData = new FormData();
    formData.append('eventID', eventID);
    let res = await fetch(`/voteForVenue/${venueID}`, {
        method: "POST",
        body: formData,
    })
    drawVenueSkills(eventID);
}

async function voteAgainstVenue(venueID){
    let eventID = document.querySelector("#event-page").getAttribute('data-id');
    let formData = new FormData();
    formData.append('eventID', eventID);
    let res = await fetch(`/voteAgainstVenue/${venueID}`, {
        method: "POST",
        body: formData,
    })
    drawVenueSkills(eventID);
}

// vvvvvvv chat related vvvvvvvvvvvvv
function addListenerToChat(){
    document.querySelector('#chat-form')
        .addEventListener('submit', async (event)=>{
            event.preventDefault();
            
            let eventID = document.querySelector("#event-page").getAttribute('data-id');
            const form = event.target;
            const formData = new FormData(form);
            
            let res = await fetch(`/message/${eventID}`, {
                method: "POST",
                body: formData,
            })
            document.querySelector('#chat-input').value = "";
            loadMessages(eventID);
        })
}

async function loadMessages(eventID){
    let chatSection = document.querySelector("#chat-section");
    let res = await fetch(`/message/${eventID}`);
    let messages = await res.json();
    console.log(messages);
    chatSection.innerHTML = "";

    for (let message of messages){
        let date = new Date(message.created_at);
        let offset = date.getTimezoneOffset();

        if (message.sender) {
            chatSection.innerHTML += `
            <div class="chat-row-self" data-id="${message.id}">
                <div class="user-info-and-content>
                    <div class="name-and-email">
                        <div class="chat-name">
                            You
                        </div>
                        <div class="chat-email">
                            ${message.email}
                        </div>
                        <div class="chat-content">
                            ${message.content}
                        </div>
                    </div>   

                    <div class="chat-time">
                        ${(date + offset).slice(16, 21)}
                    </div>
                </div>
            </div>
            `
        } else {
            chatSection.innerHTML += `
            <div class="chat-row" data-id="${message.id}">
                <div class="user-info-and-content>
                    <div class="name-and-email">
                        <div class="chat-name">
                            ${message.name}
                        </div>
                        <div class="chat-email">
                            ${message.email}
                        </div>
                        <div class="chat-content">
                            ${message.content}
                        </div>
                    </div>   

                    <div class="chat-time">
                        ${(date + offset).slice(16, 21)}
                    </div>
                </div>
            </div>

            `
        }
    }
}


               // <div class="chat-time">
                //     fix time
                // </div>


                    // <div class="chat-content>
                    //     ${message.content}
                    // </div>



// <div class="chat-row-self" data-id="${message.id}">
// <div class="user-info">
//     <div class="name-and-email">
//         <div class="user-name">${message.name}</div>
//         <div class="user-email">${message.email}</div>
//     </div>
//     <div class="message-time">
//     ${(date + offset).slice(0, 15)}
//     <br>
//     ${(date + offset).slice(16, 21)}
//     </div>
// </div>
// <div class="chat-message">
//     <div class="message-content">${message.content}</div>   
// </div>
// </div>



//<div class="chat-row" data-id="${message.id}">
//<div class="user-info">
//    <div class="name-and-email">
//        <div class="user-name">${message.name}</div>
//        <div class="user-email">${message.email}</div>
//    </div>
//    <div class="message-time">
//    ${(date + offset).slice(0, 15)}
//    <br>
//    ${(date + offset).slice(16, 21)}
//    </div>
//</div>
//<div class="chat-message">
//    <div class="message-content">${message.content}</div>   
//</div>
//</div>







// ${new Date(message.created_at).getTimezoneOffset().slice(0, 10)}
//                     <br>
//                     ${new Date(message.created_at).getTimezoneOffset().slice(11, 16)}

// ^^^^^^^^ chat related ^^^^^^^^^^^^


// vvvvvvvv event title vvvvvvvvv

async function loadEventInfos(){
    let eventID = document.querySelector('#event-page').getAttribute('data-id');
    let res = await fetch(`/eventFdList/${eventID}`);
    let fdList = await res.json();
    let container = document.querySelector("#event-title-content > .event-title")
    let response = await fetch(`/event-pic/${eventID}`);
    let path = await response.json();
    if (path.picture != null){
        document.querySelector("#event-pic").innerHTML = `
        <img src="${path.picture}">
        <form id="change-event-pic">
            <input type="file" name="event-pic" accept="image/*" onchange="newEventPic()">
        </form>
        `;
    }

    container.innerHTML = "";

    for (let friend of fdList){
        if (!friend.profile_picture){
            container.innerHTML += `
            <div class="user-row">
                <div class="user-pic"><img src="/user0.jpg"></div>
                <span>${friend.name}</span>
            </div>
            `;
        } else {
            container.innerHTML += `
            <div class="user-row">
                <div class="user-pic"><img src="/${friend.profile_picture}"></div>
                <span>${friend.name}</span>
            </div>
            `;
        }
    }
}

async function newEventPic(){
    console.log("start")
    const body = new FormData(document.querySelector('#change-event-pic'))
    let res = await fetch('/upload-event-pic', {
        method: 'POST',
        body: body,
    })
    let path = await res.text();
    console.log(path)
    document.querySelector("#crop-profile-pic").classList.add("active");
    let image = document.createElement('img');
    image.src = path;
    document.querySelector("#picture-canvas").appendChild(image);
    cropEventPic(image);
}

function cropEventPic(image) {
    var button = document.querySelector('#crop');
    var result = document.querySelector('#result');
    var croppable = false;
    var cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 1,
        ready: function () {
            croppable = true;
        },
    });

    button.onclick = function() {
        var croppedCanvas;
        var roundedCanvas;
        var roundedImage;

        if (!croppable) {
            return;
        }

        // Crop
        croppedCanvas = cropper.getCroppedCanvas();

        // Round
        roundedCanvas = getRoundedCanvas(croppedCanvas);
        roundedCanvas.toBlob(async(blob) => {
            let formData = new FormData();
            let eventID = document.querySelector('#event-page').getAttribute("data-id");
            formData.append('event-pic', blob);
            formData.append('eventID', eventID);
            let res = await fetch('/upload-event-pic', {
                method: 'POST',
                body: formData,
            })
            let path = await res.text();
            console.log(path)
            //   result.innerHTML = '';
            //   result.appendChild(roundedImage);
            document.querySelector("#crop-profile-pic").classList.remove("active");
            document.querySelector("#event-pic").innerHTML = `
            <img src="${path}">
            <form id="change-event-pic">
                <input type="file" name="event-pic" accept="image/*" onchange="newEventPic()">
            </form>
            `;
            document.querySelector('#picture-canvas').innerHTML = "";
        })
    }
}

function triggerSearchInEvent(){
    document.querySelector("#search-in-event").classList.add("active");
}

function closeSearchingDiv(){
    document.querySelector("#search-in-event").classList.remove("active");
}

async function addVenue(venueID){
    let eventID = document.querySelector('#event-page').getAttribute("data-id");
    let res = await fetch(`/addVenue/${eventID}/${venueID}`, {method: "POST"});
    let json = await res.json();
    if (json.result){
        drawVenueSkills(eventID);
        document.querySelector("#search-in-event > div > .notification").innerHTML = `
        <span style="color: var(--mainText)">Venue added.</span>`
    } else {
        document.querySelector("#search-in-event > div > .notification").innerHTML = `
        <span style="color: var(--mainText)">Venue already exists.</span>`
    }

}


let frdLists = {};
async function searchFrds(){
    let input = document.querySelector("#searchFrds");
    let resultDiv = document.querySelector("#searchFrdsResults");
    
    if (!frdLists.resYouAcc && !frdLists.resYouReq){
        let res = await fetch('/showAcceptedFds');
        let json = await res.json();
        frdLists = json;
    }
    console.log(frdLists)

    resultDiv.innerHTML = "";
    for(let key in frdLists){
        for(let frd of frdLists[key]){
            if (input.value && frd.rows[0].name.includes(input.value)){
                resultDiv.innerHTML += `
                <span onclick="addFriendToEvent(${frd.rows[0].id})">${frd.rows[0].name}</span>
                `;
            }
        }

    }
}

async function addFriendToEvent(userID){
    let eventID = document.querySelector("#event-page").getAttribute("data-id");
    let res = await fetch(`/addFriendToEvent/${userID}/${eventID}`, {method: "POST"});
    let json = await res.json();
    if (json.result){
        loadEventInfos();
    } else {
        document.querySelector("#searchFrdsResults").innerHTML = `
        <span style="color: white; font-size: 10px;">Friend already exists in this event.</span>
        `
    }
}

function eventPageSocketIO(eventID){
    console.log("SOCKET IO START");
    socket.on(`msg`, (msg)=>{
        if (msg.type == "event" && msg.eventID == eventID){
            loadMessages(eventID)
        }
    })
}