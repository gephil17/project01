async function loadPMPage(loadBtn) {
    let fdRows = document.querySelectorAll(loadBtn)
    for (let fdRow of fdRows) {
        fdRow.addEventListener('click', async function(event) {
            // console.log(`this is fd id ${event.currentTarget.parentNode.id}`)
            console.log(`this is fd id ${event.target.parentNode.id}`)
            let res = await fetch(`/pmChat`);
            let pmChatPage = await res.text()

            document.querySelector('#main').innerHTML = pmChatPage
            document.querySelector('.pm-name').innerHTML = event.target.parentNode.querySelector('.friend-name').innerHTML
            document.querySelector('.fa-arrow-left').addEventListener('click', async function() {
                let previousPage = await fetch(`/loadFdsPage`);
                let loadFdsPage = await previousPage.text()
                document.querySelector('#main').innerHTML = loadFdsPage
                showAcceptedFds()
            })

            loadPM(event.target.parentNode.id)
            sdPM(event.target.parentNode.id)
        })
    }
}

async function sdPM(fdID) {
    document.querySelector('#pm-form').addEventListener('submit', async function(event) {
        event.preventDefault();
        let yourMsg = document.querySelector("#pm-input").value

        let msgData = new FormData()
        msgData.append('msg', yourMsg)
        let res = await fetch(`/sdPM/${fdID}`, {
            method: "POST",
            body: msgData
        })
        console.log(await res.json())
        let date = new Date()
        // console.log(date)
        let offset = date.getTimezoneOffset()
        document.querySelector('#pm-input').value = "";
        
        document.querySelector('.pm-section-container').innerHTML += `
        <div class="pm-row-self">
            <div class="pm-msg">
                ${yourMsg}
            </div>
            <div class="pm-time">
                 ${(date + offset).slice(16, 21)}
            </div>
        </div>
        `

        // loadPM(fdID);
    })
}

async function loadPM(fdID) {
    let loadMsg = await fetch(`/loadPM/${fdID}`)
    let allMsg = await loadMsg.json()
    // console.log(allMsg.rows)
    let displayMsg = document.querySelector('.pm-section-container')
    displayMsg.classList.add(`yourFd${fdID}`)

   

    displayMsg.innerHTML = ''
    for (let eachMsg of allMsg.rows) {
        let date = new Date(eachMsg.created_at)
        // console.log(date)
        let offset = date.getTimezoneOffset()
        // console.log(offset)
        if (eachMsg.sender == "you") {
            // console.log(date+offset)
            displayMsg.innerHTML += `
            <div class="pm-row-self">
                <div class="pm-msg">
                    ${eachMsg.content}
                </div>
                <div class="pm-time">
                    ${(date + offset).slice(16, 21)}
                </div>
            </div>`
        } else {
            displayMsg.innerHTML +=  `
            <div class="pm-row">
                <div class="pm-msg">
                    ${eachMsg.content}
                </div>
                <div class="pm-time">
                    ${(date + offset).slice(16, 21)}
                </div>
            </div>`
        }
    }

    socket.on(`msg`,(msg)=>{
    let displayMsg = document.querySelector(`.yourFd${fdID}`)
    let date = new Date()
        // console.log(date)
    let offset = date.getTimezoneOffset();
    if (msg.type == "pm" && msg.friendID == fdID){
        displayMsg.innerHTML +=  `
                <div class="pm-row">
                    <div class="pm-msg">
                        ${msg.content}
                    </div>
                    <div class="pm-time">
                    ${(date + offset).slice(16, 21)}
                    </div>
                </div>`
    }});
    
}


/////////////////////////socketIO
const socket = io.connect();

///////////////////////////////
