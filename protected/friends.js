// function loadChats() {
//     for (let chat of chats) {
//         document.querySelector('#chats').innerHTML += `
//             <div class="chat-row">
//                 <div class="chat-pic">
//                     <img src="/${chat.pic}">
//                 </div>
//                 <div class="chat-content">
//                     <div class="chat-name">
//                         ${chat.name}
//                     </div>
//                     <div class="latest-chat-user">
//                         ${chat.user}
//                     </div>
//                     <div class="latest-chat-message">
//                         ${chat.message}
//                     </div>
//                 </div>
//                 <div class="latest-chat-time">
//                     ${chat.time}
//                 </div>
//             </div>
//         `
//     }
// }

// let chats = [];
// for (i = 0; i < 5; i++) {
//     chats[i] = {
//         pic: `user${i}.jpg`,
//         name: `Friend ${i}`,
//     };
// }

function showModalAddFds() {
    const modal = document.querySelector("#myModal-fds");
    modal.style.display = "block";
    let closeButton = document.querySelector(".close-modal-fds")
    closeButton.addEventListener('click', function () {
        modal.style.display = "none";
    })
    window.addEventListener('click', function (clickPlace) {
        if (clickPlace.target == modal) {
            modal.style.display = "none";
        }
    })
}

async function searchUsersAndSdFdReq() {
    let searchContent = document.querySelector('.input-Fds').value;
    let res = await fetch(`/searchUsers/${searchContent}`)
    let result = await res.json()
    // console.log(result)
    document.querySelector(".searchFds-results-create").innerHTML = ``
    for (let stranger of result.resStrangers) {
        document.querySelector(".searchFds-results-create").innerHTML += `
            <div class="fds-display">
                <div class="one-row">
                    <i class="fas fa-plus-circle add-logo"></i>
                    <div id="${stranger[0].id}" class="addFd-result-name">${stranger[0].name}</div>
                </div>
            </div>
            `
    }
    for (let friend of result.resFriends) {
        document.querySelector(".searchFds-results-create").innerHTML += `
            <div class="fds-display">
                <div class="one-row-friend">
                    <div class="fd-logo">Friend</div>
                    <div id="${friend[0].id}" class="addFd-result-name">${friend[0].name}</div>
                </div>
            </div>
            `
    }

    let strangers = document.querySelectorAll('.one-row')
    for (let stranger of strangers) {
        stranger.addEventListener('click', async function (event) {
            console.log(`${event.currentTarget.querySelector('div').id}`)
            let addFds = new FormData();
            addFds.append('wantToAdd', event.currentTarget.querySelector('div').id)
            let res = await fetch('/addFds', {
                method: "POST",
                body: addFds
            })
            console.log(stranger.innerHTML)
            stranger.innerHTML = `<div class="reqSent">Friend Request Sent</div>`
        })
    }
}



function showFdRequest() {
    const modal = document.querySelector("#myModal-request");
    modal.style.display = "block";
    let closeButton = document.querySelector(".close-request-modal")
    closeButton.addEventListener('click', function () {
        modal.style.display = "none";
    })
    window.addEventListener('click', function (clickPlace) {
        if (clickPlace.target == modal) {
            modal.style.display = "none";
        }
    })
}

async function checkFdRequest() {
    let res = await fetch('/checkFdRequest')
    let results = await res.json()
    // console.log(results)
    if (results.length > 0) {
        document.querySelector(".hv-request").style.display = "flex"
        document.querySelector(".no-request").style.display = "none"
    } else {
        document.querySelector(".hv-request").style.display = "none"
        document.querySelector(".no-request").style.display = "flex"
    }

    let showAdderContainer = document.querySelector("#myModal-request > div > div > div.modal-body-create > div.create-container-fdReq")
    showAdderContainer.innerHTML = ''
    for (let result of results) {
        showAdderContainer.innerHTML += `
                                        <div data-id="${result.id}" class="showFdsRequest"> 
                                            <div class="adder">${result.name}</div>
                                            <div class="accept-or-decline">
                                                <div class="action-button accept" onclick="acceptFdReq(${result.id})">Accept</div>
                                                <div class="action-button decline" onclick="declineFdReq(${result.id})">Decline</div>
                                            </div>
                                        </div>    
                                        `
    }
}

async function acceptFdReq(para) {
    await fetch(`/acceptFdReq/${para}`)
    document.querySelector(`.showFdsRequest[data-id="${para}"]`).innerHTML = 'Request accepted'
    showAcceptedFds();
    checkFdRequest();
    console.log("remove")
}

async function declineFdReq(para) {
    await fetch(`/declineFdReq/${para}`)
    document.querySelector(`.showFdsRequest[data-id="${para}"]`).innerHTML = 'Request declined'
    checkFdRequest();
    console.log("remove")
}

async function showAcceptedFds() {
    let res = await fetch(`/showAcceptedFds`)
    let result = await res.json()
    // console.log(result)
    document.querySelector('.friend-row').innerHTML = ''
    for (let accountAccepted of result.resYouAcc) {
        if (accountAccepted.rows[0].profile_picture) {
            document.querySelector('.friend-row').innerHTML += `
            <div id="${accountAccepted.rows[0].id}" class="each-fd-row">
                <div class="friend-pic">
                    <img src="/${accountAccepted.rows[0].profile_picture}">
                </div>
                <div class="friend-name">
                    ${accountAccepted.rows[0].name}
                </div>
                <div class="chat-fd fd-button">
                    Chat
                </div>
                <div class="remove-confirm" style="display: none">
                    <div class="options option-yes confirm-unfriend">Yes</div>
                    <div class="options option-no">No</div>
                </div>
                <div class="remove-fd fd-button">
                    Remove 
                </div>
            </div>
            `
        } else {
            document.querySelector('.friend-row').innerHTML += `
            <div id="${accountAccepted.rows[0].id}" class="each-fd-row">
                <div class="friend-pic">
                    <img src="/pepe.jpg">
                </div>
                <div class="friend-name">
                    ${accountAccepted.rows[0].name}
                </div>
                <div class="chat-fd fd-button">
                    Chat
                </div>
                <div class="remove-confirm" style="display: none">
                    <div class="options option-yes confirm-unfriend">Yes</div>
                    <div class="options option-no">No</div>
                </div>
                <div class="remove-fd fd-button">
                    Remove 
                </div>
            </div>
            `
        }
    }

    for (let accountRequested of result.resYouReq) {
        if (accountRequested.rows[0].profile_picture) {
            document.querySelector('.friend-row').innerHTML += `
            <div id="${accountRequested.rows[0].id}" class="each-fd-row">
                <div class="friend-pic">
                    <img src="/${accountRequested.rows[0].profile_picture}">
                </div>
                <div class="friend-name">
                    ${accountRequested.rows[0].name}
                </div>
                <div class="chat-fd fd-button">
                    Chat
                </div>
                <div class="remove-confirm" style="display: none">
                    <div class="options option-yes confirm-unfriend">Yes</div>
                    <div class="options option-no">No</div>
                </div>
                <div class="remove-fd fd-button">
                    Remove 
                </div>
            </div>
            `
        } else {
            document.querySelector('.friend-row').innerHTML += `
            <div id="${accountRequested.rows[0].id}" class="each-fd-row">
            <div class="friend-pic">
                <img src="/pepe.jpg">
            </div>
            <div class="friend-name">
                ${accountRequested.rows[0].name}
            </div>
            <div class="chat-fd fd-button">
                Chat
            </div>
            <div class="remove-confirm" style="display: none">
                <div class="options option-yes confirm-unfriend">Yes</div>
                <div class="options option-no">No</div>
            </div>
            <div class="remove-fd fd-button">
                Remove 
            </div>
            </div>
            `
        }

    }

    removeFd(".remove-fd")
    cancelUnfriend(".option-no")
    confirmUnfriend(".confirm-unfriend")
    loadPMPage(".chat-fd")
}

function removeFd(para) {
    let removeButtons = document.querySelectorAll(para)
    for (let removeButton of removeButtons) {
        removeButton.addEventListener("click", function (event) {
            console.log('this is function removeFD()')
            event.target.style.display = "none"
            event.target.parentNode.querySelector('.remove-confirm').style.display = ""
        })
    }
}

function cancelUnfriend(para) {
    let noButtons = document.querySelectorAll(para)
    for (let noButton of noButtons) {
        noButton.addEventListener("click", function (event) {
            console.log('this is function cancelUnfriend()')
            event.target.parentNode.parentNode.querySelector(".remove-fd").style.display = ""
            event.target.parentNode.style.display = "none"
        })
    }
}

function confirmUnfriend(para) {
    let confirmFdButtons = document.querySelectorAll(para)
    for (let confirmFdButton of confirmFdButtons) {
        confirmFdButton.addEventListener("click", async function (event) {
            console.log('this is function confirmUnfriend()')
            console.log(`you remove this fd with id: ${event.currentTarget.parentNode.parentNode.id}`)
            console.log(event.target.parentNode.parentNode.innerHTML)
            await fetch(`/unfriend/${event.currentTarget.parentNode.parentNode.id}`)
            console.log('you remove a fd')
            event.target.parentNode.parentNode.innerHTML = `<div class="unfriended">Unfriended</div>`
        })
    }
}