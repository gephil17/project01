async function loadEvents(){
    let res = await fetch('/loadEvents');
    let events = await res.json();
    let eventSection = document.querySelector('#events');
    eventSection.innerHTML = "";
    for (let event of events){
        let response = await fetch(`/latest-message/${event.event_id}`);
        let message = await response.json();
            
        if (message.content){
            let date = new Date(message.created_at);
            let offset = date.getTimezoneOffset();
            if (event.picture){
                eventSection.innerHTML += `
                <div class="event-row" onclick="toEvent('${event.event_id}')">
                    <div class="event-pic">
                        <img src="${event.picture}">
                    </div>
                    <div class="event-content">
                        <div class="event-name">
                            ${event.name}
                        </div>
                        <div class="latest-chat-message">
                            ${message.name}:  ${message.content}
                        </div>
                    </div>
                    <div class="latest-chat-time">
                        ${(date + offset).slice(0, 15)}
                        <br>
                        ${(date + offset).slice(16, 21)}
                    </div>
                </div>
            `
            } else {
                eventSection.innerHTML += `
                    <div class="event-row" onclick="toEvent('${event.event_id}')">
                        <div class="event-pic">
                            <img src="pepe.jpg">
                        </div>
                        <div class="event-content">
                            <div class="event-name">
                                ${event.name}
                            </div>
                            <div class="latest-chat-message">
                                ${message.name}:  ${message.content}
                            </div>
                        </div>
                        <div class="latest-chat-time">
                            ${(date + offset).slice(0, 15)}
                            <br>
                            ${(date + offset).slice(16, 21)}
                        </div>
                    </div>
                `
            }
        } else {
            if (event.picture){
                eventSection.innerHTML += `
                <div class="event-row" onclick="toEvent('${event.event_id}')">
                    <div class="event-pic">
                        <img src="${event.picture}">
                    </div>
                    <div class="event-content">
                        <div class="event-name">
                            ${event.name}
                        </div>
                </div>
            `
            } else {
            eventSection.innerHTML += `
                <div class="event-row" onclick="toEvent('${event.event_id}')">
                    <div class="event-pic">
                        <img src="pepe.jpg">
                    </div>
                    <div class="event-content">
                        <div class="event-name">
                            ${event.name}
                        </div>
                </div>
            `
            }
        }
    }
}

// let events = [];
// for (i = 0; i < 5; i++){
//     events[i] = {
//         pic: `user${i}.jpg`,
//         name: `Event ${i}`,
//         user: `user${i}`,
//         message: `message${i}`,
//         time: "Date<br>Time"
//     };
// }

async function toEvent(eventID){
    let res = await fetch(`/event`);
    let html = await res.text()
    document.querySelector('#main').innerHTML = html;
    toToday();
    
    let event = await fetch(`/event/${eventID}`);
    let json = await event.json();
    
    document.querySelector('#event-page').setAttribute("data-id", json.id);
    document.querySelector('.event-page-title').innerHTML = json.name + ` <i class="fas fa-angle-down"></i>`; /* change to event name */
    document.querySelector('#title-name').innerText = "Event";
    drawDateSkills(eventID);
    drawVenueSkills(eventID);
    loadMessages(eventID)
    addListenerToEventDates();
    addListenerToChat();
    loadEventInfos();
    eventPageSocketIO(eventID);
}

