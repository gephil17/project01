// async function toCalendar(){
//     let res = await fetch('/calendar');
//     let html = await res.text();
//     document.querySelector('#main').innerHTML = html;
// }

toPage('calendar');

async function toPage(req) {
    let res = await fetch(req);
    // console.log(res)
    let html = await res.text();
    // console.log(html)
    document.querySelector('#main').innerHTML = html;
    document.querySelectorAll('#bottom-panel > *').forEach(el => el.classList.remove('selected'));
    document.querySelector(`#to-${req}`).classList.add('selected');
    document.querySelector('#title-name').innerText = req.charAt(0).toUpperCase() + req.slice(1);
    document.querySelector('#menu').classList.remove("active");
    if (req == "events") {
        loadEvents();
    } else if (req == "calendar") {
        toToday();
        loadLatestEvents();
    } else if (req == "friends") {
        checkFdRequest();
        showAcceptedFds();
    }
}

function triggerOptions() {
    let menu = document.querySelector('#menu');
    if (menu.classList.contains("active")) {
        menu.classList.remove("active");
    } else {
        menu.classList.add("active");
    }
    // document.querySelector('#trigger').classList.push("active");
}

async function signOut() {
    let res = await fetch('/sign-out');
}

async function loadUserInfos() {
    let res = await fetch('/user-infos');
    let json = await res.json();
    document.querySelector('.user-name').innerText = json.name;
    document.querySelector('.user-email').innerText = json.email;
    document.querySelector('#profile-pic').setAttribute("data-id", json.id);
    // console.log(json);
    if(json.profile_picture){
        document.querySelector('#user-profile-pic').src = json.profile_picture;
    }
}

loadUserInfos();



//////////////// dark mode switch

function switchMode() {
    //const searchInput = document.getElementById("search-input")
    const toggleSwitch = document.querySelector('.switchBtn');
    if (toggleSwitch.classList.contains("light")) {
        document.documentElement.setAttribute('data-theme', 'dark');
        toggleSwitch.classList.remove("light")
    }
    else {
        document.documentElement.setAttribute('data-theme', 'light');
        toggleSwitch.classList.add("light")
    }
}

// toggleSwitch.addEventListener('click', switchMode);

// vvvvv options bar related vvvvvvv

async function newProfilePic() {
    console.log("start")
    const body = new FormData(document.querySelector('#profile-pic'))
    let res = await fetch('/upload-profile-pic', {
        method: 'POST',
        body: body,
    })
    let path = await res.text();
    console.log(path)
    document.querySelector("#crop-profile-pic").classList.add("active");
    let image = document.createElement('img');
    image.src = path;
    document.querySelector("#picture-canvas").appendChild(image);
    cropProfilePic(image);

}



function getRoundedCanvas(sourceCanvas) {
    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    var width = sourceCanvas.width;
    var height = sourceCanvas.height;

    canvas.width = width;
    canvas.height = height;
    context.imageSmoothingEnabled = true;
    context.drawImage(sourceCanvas, 0, 0, width, height);
    context.globalCompositeOperation = 'destination-in';
    context.beginPath();
    context.arc(width / 2, height / 2, Math.min(width, height) / 2, 0, 2 * Math.PI, true);
    context.fill();
    return canvas;
}

function cropProfilePic(image) {
    var button = document.querySelector('#crop');
    var result = document.querySelector('#result');
    var croppable = false;
    var cropper = new Cropper(image, {
        aspectRatio: 1,
        viewMode: 1,
        ready: function () {
            croppable = true;
        },
    });

    button.onclick = function() {
        var croppedCanvas;
        var roundedCanvas;
        var roundedImage;

        if (!croppable) {
            return;
        }

        // Crop
        croppedCanvas = cropper.getCroppedCanvas();

        // Round
        roundedCanvas = getRoundedCanvas(croppedCanvas);
        roundedCanvas.toBlob(async(blob) => {
            let formData = new FormData();
            let userID = document.querySelector('#profile-pic').getAttribute("data-id");
            formData.append('profile-pic', blob);
            formData.append('userID', userID);
            let res = await fetch('/upload-profile-pic', {
                method: 'POST',
                body: formData,
            })
            let path = await res.text();
            console.log(path)
            //   result.innerHTML = '';
            //   result.appendChild(roundedImage);
            document.querySelector("#crop-profile-pic").classList.remove("active");
            document.querySelector("#user-profile-pic").src = path;
            document.querySelector('#picture-canvas').innerHTML = "";
        })
    }
}

function closeCroppingDiv() {
    document.querySelector("#crop-profile-pic").classList.remove("active");
    document.querySelector("#picture-canvas").innerHTML = "";
}