async function search() {
    let searchContent = document.querySelector('#search-input').value;
    const res = await fetch(`/search/${searchContent}`, {
        method: "GET"
    })
    document.querySelector("#search-results").innerHTML = "";
    let result = await res.json();
    for (let restaurant of result) {
        document.querySelector("#search-results").innerHTML += `
        <div>${restaurant.name}</div>
        `
    }
}

async function searchByLocation() {
    let locations = document.getElementsByName("location")
    let locationsChoices = [];
    locations.forEach(function (location) {
        if (location.checked) {
            locationsChoices.push(location.value)
        }
    })

    let dishes = document.getElementsByName("dish")
    let dishesChoices = [];
    dishes.forEach(function (dishes) {
        if (dishes.checked) {
            dishesChoices.push(dishes.value)
        }
    })

    let res = await fetch(`/searchByTerritory?location=${locationsChoices}&dish=${dishesChoices}`);
    let results = await res.json()
    console.log(results)
    // for (let result of results) {

    // }
    let totalNumber = 0;
    for (let result of results) {
        totalNumber += result.length
    }


    document.querySelector("#search-results").innerHTML = `<div class="search-result">Total ${totalNumber} results</div>`
    for (let result of results) {
        for (let restaurant of result) {
            if(document.querySelector('#event-page')){
                document.querySelector("#search-results").innerHTML += `
                <div class="result-container" onclick="addVenue(${restaurant.id})">
                    <div class="result-name">${restaurant.name}</div>
                    <div class="result-address">${restaurant.address}</div>
                    <i class="fas fa-map-marker-alt" onclick="showModalSearch()"></i>
                </div>`
            } else {
                document.querySelector("#search-results").innerHTML += `
                <div class="result-container">
                    <div class="result-name">${restaurant.name}</div>
                    <div class="result-address">${restaurant.address}</div>
                    <i class="fas fa-map-marker-alt" onclick="showModalSearch()"></i>
                </div>`
            }
        }
    }
    // console.log(totalNumber)
}

async function searchByName() {
    let searchContent = document.querySelector('#search-input').value;
    if (searchContent == ''){
        document.querySelector("#search-results").innerHTML = ""
        return;
    }
    let res = await fetch(`/searchByName/${searchContent}`)
    let result = await res.json()
    // console.log(result)
    document.querySelector("#search-results").innerHTML = `<div class="search-result">Total ${result.length} results</div>`
    for (let restaurant of result) {
        if(document.querySelector('#event-page')){
            document.querySelector("#search-results").innerHTML += `
            <div class="result-container" onclick="addVenue(${restaurant.id})">
                <div class="result-name">${restaurant.name}</div>
                <div class="result-address">${restaurant.address}</div>
                <i class="fas fa-map-marker-alt" onclick="showModalSearch()"></i>
            </div>`
        } else {
            document.querySelector("#search-results").innerHTML += `
            <div class="result-container">
                <div class="result-name">${restaurant.name}</div>
                <div class="result-address">${restaurant.address}</div>
                <i class="fas fa-map-marker-alt" onclick="showModalSearch()"></i>
            </div>`
        }
        
    }
}

function showList() {
    let advanceSearch = [document.querySelector('.territory-and-districts'), document.querySelector('.submit-button')];
    advanceSearch.forEach(function (element) {
        if (element.classList.contains("no-display")) {
            element.classList.remove("no-display")
        } else {
            element.classList.add("no-display")
        }
    })
}

////用enter制黎輸入資料
// document.addEventListener('keypress', function (e) {
//     if (e.key === 'Enter') {
//         if (document.querySelector("#search-input").value) {
//             searchByName()
//         }
//     }
// })

// modal
function showModalSearch() {
    const modal = document.querySelector("#myModal-search");
    modal.style.display = "block";
    let closeButton = document.querySelector(".close-modal-search")
    closeButton.addEventListener('click', function () {
        modal.style.display = "none";
    })
    window.addEventListener('click', function (clickPlace) {
        if (clickPlace.target == modal) {
            modal.style.display = "none";
        }
    })

    // let showMap = document.querySelector("#map")
    // showMap.innerHTML += `<div>onemoetestwe</div>`
}
