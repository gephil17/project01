function loadCalendar(month, year) {
    let calendar = document.querySelector("#calendar");
    let daysInThisMonth = new Date(year, month, 0).getDate();
    let firstDayInThisMonth = new Date(year, month - 1, 1).getDay();
    let weekCount = Math.ceil((firstDayInThisMonth + daysInThisMonth) / 7);
    document.querySelector('#month-and-year').innerHTML = `${months[month - 1]} ${year}`;
    if (month == 12) {
        document.querySelector('#previous-month').setAttribute('onclick', `loadCalendar(${month - 1}, ${year})`);
        document.querySelector('#next-month').setAttribute('onclick', `loadCalendar(1, ${year + 1})`);
    } else if (month == 1) {
        document.querySelector('#previous-month').setAttribute('onclick', `loadCalendar(12, ${year - 1})`);
        document.querySelector('#next-month').setAttribute('onclick', `loadCalendar(${month + 1}, ${year})`);
    } else {
        document.querySelector('#previous-month').setAttribute('onclick', `loadCalendar(${month - 1}, ${year})`);
        document.querySelector('#next-month').setAttribute('onclick', `loadCalendar(${month + 1}, ${year})`);
    }

    calendar.querySelector(`#dates`).innerHTML = "";
    let week = [];
    for (let i = 1; i <= weekCount; i++) {
        const tr = document.createElement('tr');
        tr.id = 'week' + i;
        calendar.querySelector(`#dates`).appendChild(tr);

        // calendar.querySelector(`table > tbody`).innerHTML += `<tr id="week${i}"></tr>`;
        // console.log(document.querySelector(`#week${i}`));
        week[i] = document.querySelector(`#week${i}`);
    }

    for (let i = 0; i < firstDayInThisMonth; i++) {
        week[1].innerHTML += `<td></td>`;
    }

    for (let i = 1; i <= daysInThisMonth; i++) {
        let weekOfThisDay = Math.ceil((firstDayInThisMonth + i) / 7);
        if (i < 10) {
            week[weekOfThisDay].innerHTML += `<td class="date" id="date${year}-${month}-0${i}"><span>${i}</span></td>`;
        } else {
            week[weekOfThisDay].innerHTML += `<td class="date" id="date${year}-${month}-${i}"><span>${i}</span></td>`;
        }
    }

    if (document.querySelector('#event-page')) {
        addListenerToEventDates();
        if (document.querySelector('#event-page').getAttribute("data-id")) {
            let eventID = document.querySelector("#event-page").getAttribute("data-id");
            drawDateSkills(eventID);
        }
    } else if (document.querySelector('#calendar-page')) {
        addListenerToPersonalDates();
        let todayDate = new Date();
        let today = todayDate.toISOString().slice(0, 10);
        loadSchedule(`date${today}`)
    }
}

async function addListenerToPersonalDates() {
    let dates = document.querySelectorAll(".date");
    for (let date of dates) {
        date.setAttribute("onclick", `loadSchedule("${date.id}")`);
        date.classList.remove("scheduled");
    }

    let res = await fetch('/scheduledDates');
    let scheduledDates = await res.json();
    for (let scheduledDate of scheduledDates) {
        if (document.querySelector(`#date${scheduledDate.date}`)){
            
            document.querySelector(`#date${scheduledDate.date}`)
                .classList.add("scheduled");
        }
    }

}

async function loadSchedule(dateStr) {
    let date = dateStr.slice(4);
    let todayDate = new Date();
    let today = todayDate.toISOString().slice(0, 10);
    let yesterday = new Date(todayDate.setDate(todayDate.getDate() - 1)).toISOString().slice(0, 10);
    let tomorrow = new Date(todayDate.setDate(todayDate.getDate() + 2)).toISOString().slice(0, 10);
    let dates = document.querySelectorAll(".date");
    dates.forEach(el => el.classList.remove("scheduling"));
    document.querySelector(`#${dateStr}`).classList.add("scheduling");
    let textArea = document.querySelector("#personal-schedule > textarea");
    let boxTitle = document.querySelector("#personal-schedule > .box-title");
    if (date == today) {
        boxTitle.innerText = "Today";
    } else if (date == yesterday) {
        boxTitle.innerText = "Yesterday";
    } else if (date == tomorrow) {
        boxTitle.innerText = "Tomorrow";
    } else {
        boxTitle.innerText = dateStr.slice(4);
    }
    textArea.setAttribute("data-id", date);

    let res = await fetch(`/personalSchedule/${date}`);
    let json = await res.json();
    textArea.value = json.content;
}


function loadCalendarOnDate(dateStr) {
    let date = new Date(dateStr);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    loadCalendar(month, year);
}


function toToday() {
    let today = new Date();
    let year = today.getFullYear();
    let month = today.getMonth() + 1;
    let date = today.getDate();
    loadCalendar(month, year);

    if (date < 10){
        document.querySelector(`#date${year}-${month}-0${date}`).classList.add("today");
    } else {
        document.querySelector(`#date${year}-${month}-${date}`).classList.add("today");
    }
}

const daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


///////////// modal
function showModalCreate() {
    const modal = document.querySelector("#myModal-create");
    modal.style.display = "block";


    let closeButton = document.querySelector(".close-modal-create")
    closeButton.addEventListener('click', function () {
        modal.style.display = "none";
    })
    window.addEventListener('click', function (clickPlace) {
        if (clickPlace.target == modal) {
            modal.style.display = "none";
        }
    })
}

async function searchRestaurantCreate() {
    let searchContent = document.querySelector('.input-Restaurants').value;
    let res = await fetch(`/searchRestaurantCreate/${searchContent}`)
    let result = await res.json()
    // console.log(result)
    document.querySelector(".searchRestaurant-results-create").innerHTML = ``
    for (let restaurant of result) {
        document.querySelector(".searchRestaurant-results-create").innerHTML += `
            <div class="for-selection" onclick="chooseRestaurant(event)">
                <div id="${restaurant.id}" class="result-name-create">${restaurant.name}</div>
                <div class="result-address">${restaurant.address}</div>
            </div>
            `
    }
}

function chooseRestaurant(event) {
    event.currentTarget.remove()
    document.querySelector('.searchRestaurant-results-create').innerHTML =
        `<div class="confirmed confirmed-restaurants" onclick="del(event)">
        ${event.currentTarget.innerHTML}</div>` + document.querySelector('.searchRestaurant-results-create').innerHTML
    // console.log(document.querySelector('.pick-restaurant').innerHTML)
}

function del(event) {
    event.currentTarget.remove()
}

async function searchFdsCreate() {
    let searchContent = document.querySelector('.input-Fds').value;
    console.log("search content")
    console.log(searchContent)
    let res = await fetch(`/searchFdsCreate/${searchContent}`)
    let result = await res.json()
    console.log(result)
    let userID = document.querySelector('#profile-pic').getAttribute("data-id")
    document.querySelector(".searchFds-results-create").innerHTML = ``
    for (let fd of result) {
        console.log(fd)
        if (userID == fd.adder_id) {
            document.querySelector(".searchFds-results-create").innerHTML += `
                <div class="for-selection" onclick="chooseFd(event)">
                    <div id="${fd.receiver_id}" class="result-name-create">${fd.name}</div>
                </div>
                `
        } else {
            document.querySelector(".searchFds-results-create").innerHTML += `
            <div class="for-selection" onclick="chooseFd(event)">
                <div id="${fd.adder_id}" class="result-name-create">${fd.name}</div>
            </div>
            `
        }

    }
}

function chooseFd(event) {
    event.currentTarget.remove()
    document.querySelector('.searchFds-results-create').innerHTML =
        `<div class="confirmed confirmed-fds" onclick="del(event)" style="margin-bottom: 10px">
        ${event.currentTarget.innerHTML}</div>` + document.querySelector('.searchFds-results-create').innerHTML
    // console.log(document.querySelector('.pick-restaurant').innerHTML)
}

async function createEventDraft() {
    let fdsPickArr = []
    let restaurantsPickArr = []

    let eventName = document.querySelector("#eventName").value
    let fdsPicks = document.querySelectorAll(".confirmed-fds > .result-name-create")
    for (let fdsPick of fdsPicks) {
        fdsPickArr.push(fdsPick.id)
    }

    let restaurantsPicks = document.querySelectorAll(".confirmed-restaurants > .result-name-create")
    for (let restaurantsPick of restaurantsPicks) {
        restaurantsPickArr.push(restaurantsPick.id)
    }

    if (checkDuplicate(fdsPickArr) == false || checkDuplicate(restaurantsPickArr) == false) {
        console.log("fail to create event")
        document.querySelector(".create-alert").innerHTML = "Can't choose a friend/restaurant twice!"
        document.querySelector(".create-alert").style.display = "block"
        return;
    } else if (eventName == '' || fdsPickArr[0] == undefined || restaurantsPickArr[0] == undefined) {
        document.querySelector(".create-alert").innerHTML = "Event's details needed."
        document.querySelector(".create-alert").style.display = "block"
        console.log("need to fill in all boxes")
        return;
    } else {
        console.log("success creating event")

        let createEventDetails = new FormData();
        createEventDetails.append('eventName', eventName)
        createEventDetails.append('confirmedFds', fdsPickArr)
        createEventDetails.append('confirmedRestaurants', restaurantsPickArr)

        let res = await fetch('/createEvent', {
            method: "POST",
            body: createEventDetails
        })

        let eventID = await res.json();
        toEvent(eventID);
    }
    // console.log(res)
    // console.log(res.json())
}


function checkDuplicate(para) {
    let arrToBeChecked = para
    let result = '';

    let set = new Set(arrToBeChecked);
    console.log(arrToBeChecked.length)
    console.log(set.size)
    if (arrToBeChecked.length !== set.size) {
        result = 'duplicated';
    }
    if (result == 'duplicated') {
        return false
    }
}

async function writeSchedule() {
    let textarea = document.querySelector("#personal-schedule > textarea");
    let userID = document.querySelector("#profile-pic").getAttribute("data-id");
    let date = textarea.getAttribute("data-id");
    console.log(userID);
    console.log(date);
    console.log(textarea.value)
    let body = new FormData();
    body.append('content', textarea.value);
    body.append('date', date);
    let res = await fetch(`/personalSchedule/${userID}`, {
        method: "PUT",
        body: body,
    })

    addListenerToPersonalDates();
}

async function loadLatestEvents() {
    let res = await fetch(`/latest-events`);
    let events = await res.json();

    for (let event of events) {
        document.querySelector('#latest-events').innerHTML += `
        <div class="event-container" onclick="toEvent(${event.event_id})">
            <div class="event-name">${event.name}</div>
        </div>`
    }
}