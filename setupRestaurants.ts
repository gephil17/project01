import jsonfile from 'jsonfile';
import { Client } from 'pg';
import dotenv from 'dotenv';

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

client.connect()
    .then(() => {
        console.log(`DB ${process.env.DB_NAME} Connected.`)
    })
    .catch(err => {
        console.log("DB Disconnected.")
    })

async function importDataToDB() {   
    let readJson = await jsonfile.readFile("database/restaurant.json", "utf8")
    // console.log(readJson)
    for (let i = 0; i < readJson.length; i++) {
        await client.query(`
        INSERT INTO restaurants (name, address, style, territory, district)
        VALUES ($1, $2, $3, $4, $5)`,
        [readJson[i].name, readJson[i].address, readJson[i].style, readJson[i].territory, readJson[i].district]);
        console.log(i);
    }
    console.log("done")
}
importDataToDB()